<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DbConnection</name>
    <message>
        <location filename="../db/DbConnection.cpp" line="43"/>
        <source>Obtaining databases list</source>
        <translation>Получение списка баз данных</translation>
    </message>
    <message>
        <location filename="../db/DbConnection.cpp" line="48"/>
        <source>Database %1 creation</source>
        <translation>Создания базы данных %1</translation>
    </message>
    <message>
        <location filename="../db/DbConnection.cpp" line="69"/>
        <source>Obtaining database schemas list</source>
        <translation>Получение списка схем базы данных</translation>
    </message>
    <message>
        <location filename="../db/DbConnection.cpp" line="74"/>
        <source>Database schema %1 creation</source>
        <translation>Создание схемы базы данных %1</translation>
    </message>
</context>
<context>
    <name>DbElementSerializer</name>
    <message>
        <location filename="../db/DbElementSerializer.cpp" line="29"/>
        <source>Save elements to database %1</source>
        <translation>Сохранение эементов в базу данных %1</translation>
    </message>
    <message>
        <location filename="../db/DbElementSerializer.cpp" line="49"/>
        <source>Element deletion from table %1</source>
        <translation>Удаление элементов из таблицы %1</translation>
    </message>
</context>
<context>
    <name>DbQuery</name>
    <message>
        <location filename="../db/DbQuery.cpp" line="25"/>
        <source>Cannot prepare operation &apos;%1&apos;! Database error: %2. Query: %3</source>
        <oldsource>Cannot prepare operation &apos;%1&apos;! Database error: %2</oldsource>
        <translation>Не удалось подготовить операцию &apos;%1&apos;! Ошибка базы данных: %2. Запрос %3</translation>
    </message>
    <message>
        <location filename="../db/DbQuery.cpp" line="46"/>
        <source>Cannot execute operation &apos;%1&apos;! Database error: %2</source>
        <translation>Не удалось выполнить операцию &apos;%1&apos;! Ошибка базы данных: %2</translation>
    </message>
    <message>
        <location filename="../db/DbQuery.cpp" line="67"/>
        <source>Cannot finish operation &apos;%1&apos;! Database error: %2</source>
        <translation>Не удалось завершить операцию &apos;%1&apos;! Ошибка базы данных %2</translation>
    </message>
</context>
<context>
    <name>DbStdErrors</name>
    <message>
        <location filename="../db/DbStdErrors.cpp" line="10"/>
        <source>Not supported database type: %1</source>
        <translation>Неподдерживаемый тип базы данных: %1</translation>
    </message>
    <message>
        <location filename="../db/DbStdErrors.cpp" line="14"/>
        <source>Database %1 not exists</source>
        <translation>Баз данных %1 не существует</translation>
    </message>
    <message>
        <location filename="../db/DbStdErrors.cpp" line="18"/>
        <source>Database schema %1 not exists</source>
        <translation>Схема базы данных %1 не существует</translation>
    </message>
    <message>
        <location filename="../db/DbStdErrors.cpp" line="22"/>
        <source>Cannot open database connection: %1</source>
        <translation>Не удалось открыть соединение с базой данных: %1</translation>
    </message>
    <message>
        <location filename="../db/DbStdErrors.cpp" line="26"/>
        <source>Database structure version is not supported! Update your software!</source>
        <translation>Неподдерживаемая версия структуры базы данных! Обновите Ваше программное обеспечение!</translation>
    </message>
    <message>
        <location filename="../db/DbStdErrors.cpp" line="30"/>
        <source>Database connection is not open!</source>
        <translation>Не установлено соединение с базой данных!</translation>
    </message>
</context>
<context>
    <name>DbStdIdElementSerializer</name>
    <message>
        <location filename="../db/elems/stdid/DbStdIdElementSerializer.cpp" line="42"/>
        <source>Getting last element id from table %1</source>
        <translation>Получение идентификатора последнего элемента из таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/elems/stdid/DbStdIdElementSerializer.cpp" line="67"/>
        <source>Cannot get last record inserted id</source>
        <translation>Не удалось получить идентификатор последнего добавленного элемента</translation>
    </message>
</context>
<context>
    <name>DbTypeAdapter</name>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="54"/>
        <source>Obtaining database table list</source>
        <translation>Получение списка таблиц базы данных</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="80"/>
        <source>Obtaining column list of table %1</source>
        <translation>Получение списка столбцов таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="163"/>
        <source>Obtaining index list from table %1</source>
        <translation>Получение списка индексов из таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="195"/>
        <source>Create index %1 in table %2</source>
        <translation>Создание индекса %1 в таблице %2</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="235"/>
        <source>Table creation %1</source>
        <translation>Создание таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="251"/>
        <source>Table deletion %1</source>
        <translation>Удаление таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="353"/>
        <source>Create foreign key %1</source>
        <translation>Создание внешнего ключа %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="361"/>
        <source>Drop foreign key %1</source>
        <translation>Удаление внешнего ключа %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="379"/>
        <source>Create index %1 on table %2</source>
        <oldsource>Create index %1 on table</oldsource>
        <translation>Создание индекса %1 таблицы %2</translation>
    </message>
    <message>
        <source>Index of table %1 creation</source>
        <translation type="vanished">Создание идекса таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="281"/>
        <source>Addition column to table %1</source>
        <translation>Добавление столбца к таблице %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="304"/>
        <source>Columns removal from table %1</source>
        <translation>Удаление столбца из таблицы %1</translation>
    </message>
    <message>
        <source>Table %1 removal</source>
        <translation type="vanished">Удаление таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="323"/>
        <source>Obtaining column %2 of table %1 constrains</source>
        <translation>Получение ограничений столбца %2 таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapter.cpp" line="335"/>
        <source>Removal constrain %2 from table %1</source>
        <translation>Удаление ограничения %2 из таблицы %1</translation>
    </message>
</context>
<context>
    <name>DbTypeAdapterMSSQL</name>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMSSQL.cpp" line="92"/>
        <source>Lock %1 acquire</source>
        <translation>Захват блокировки %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMSSQL.cpp" line="104"/>
        <source>Lock %1 release</source>
        <translation>Освобождение блокировки %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMSSQL.cpp" line="96"/>
        <source>Lock %1 acquire timeout</source>
        <oldsource>Lock acquire timeout</oldsource>
        <translation>Истекло время захвата блокировки %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMSSQL.cpp" line="121"/>
        <source>Column %1 rename from table %2</source>
        <translation>Переименование столбца %1 из таблицы %2</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMSSQL.cpp" line="131"/>
        <source>Change column %2 type of table %1</source>
        <translation>Изменение типа столбца %2 таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMSSQL.cpp" line="139"/>
        <source>Table %1 rename to %2</source>
        <translation>Переименование таблицы %1 в %2</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMSSQL.h" line="13"/>
        <source>Microsoft SQL Server</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DbTypeAdapterMySQL</name>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.cpp" line="105"/>
        <source>Lock %1 acquire</source>
        <translation>Захват блокировки %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.cpp" line="110"/>
        <source>Lock %1 release</source>
        <translation>Освобождение блокировки %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.cpp" line="127"/>
        <source>Column %1 rename</source>
        <translation>Переименование столбца %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.cpp" line="146"/>
        <source>Addition columns to table %1</source>
        <oldsource>Addtion columns to table %1</oldsource>
        <translation>Добавление столбца к таблице %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.cpp" line="169"/>
        <source>Removal columns from table %1</source>
        <translation>Удаление столбца из таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.cpp" line="180"/>
        <source>Disable foreing key for auto id column %1</source>
        <translation>Запрещение внешних ключей для автоинкрементируемого поля %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.cpp" line="189"/>
        <source>Change column %2 type of table %1</source>
        <translation>Изменение типа столбца %2 таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.cpp" line="196"/>
        <source>Enable foreing key for auto id column %1</source>
        <translation>Разрешение внешних ключей для автоинкрементируемого поля %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.cpp" line="205"/>
        <source>Rename table from %1 to %2</source>
        <translation>Переименование таблицы из %1 в %2</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterMySQL.h" line="13"/>
        <source>MariaDB/MySQL</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DbTypeAdapterPostgreSQL</name>
    <message>
        <location filename="../db/adapters/DbTypeAdapterPostgreSQL.cpp" line="108"/>
        <source>Lock %1 acquire</source>
        <translation>Захват блокировки %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterPostgreSQL.cpp" line="114"/>
        <source>Lock %1 release</source>
        <translation>Освобождение блокировки %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterPostgreSQL.cpp" line="133"/>
        <source>Column %1 rename</source>
        <translation>Переименование столбца %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterPostgreSQL.cpp" line="144"/>
        <source>Addition column to table %1</source>
        <translation>Добавление столбца к таблице %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterPostgreSQL.cpp" line="160"/>
        <source>Removal column %2 from table %1</source>
        <translation>Удаление столбца %2 из таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterPostgreSQL.cpp" line="200"/>
        <source>Change column %2 type of table %1</source>
        <translation>Изменение типа столбца %2 таблицы %1</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterPostgreSQL.cpp" line="208"/>
        <source>Rename table from %1 to %2</source>
        <translation>Переименование таблицы из %1 в %2</translation>
    </message>
    <message>
        <location filename="../db/adapters/DbTypeAdapterPostgreSQL.h" line="12"/>
        <source>PostgreSQL</source>
        <translation></translation>
    </message>
</context>
</TS>
