#include "DbColumnInfo.h"
#include "db/DbQuery.h"


QString DbColumnInfo::fieldNamesString() {
    return QString("%1,%2,%3,%4,%5,%6")
            .arg(fieldNameSchema(),
                fieldNameTableName(),
                fieldNameColumnName(),
                fieldNameDefaultValue(),
                fieldNameDataType(),
                fieldNameExtra());
}

DbColumnInfo::DbColumnInfo(const DbQuery &query) {
    int pos {0};
    m_schema = query.getString(pos++);
    m_tableName = query.getString(pos++);
    m_name = query.getString(pos++);
    m_default = query.getString(pos++);
    m_type = query.getString(pos++);
    m_extra = query.getString(pos++);
}
