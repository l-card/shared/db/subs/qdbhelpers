#include "DbConnectionParams.h"

DbConnectionParams::DbConnectionParams(const QString &dbType, const QString &srv, const QString &db,
                                       const QString &scheme, const QString &user, const QString &passwd,
                                       int port) :
    m_dbType{dbType},
    m_server{srv},
    m_database{db},
    m_schema{scheme},
    m_user{user},
    m_passwd{passwd},
    m_port{port} {

}

DbConnectionParams::DbConnectionParams(const DbConnectionParams &params) :
    m_dbType{params.m_dbType},
    m_server{params.m_server},
    m_database{params.m_database},
    m_schema{params.m_schema},
    m_user{params.m_user},
    m_passwd{params.m_passwd},
    m_port{params.m_port} {

}

DbConnectionParams::DbConnectionParams()  {

}

DbConnectionParams &DbConnectionParams::operator=(const DbConnectionParams &other) {
    if (this != &other) {
        m_dbType = other.m_dbType;
        m_server = other.m_server;
        m_database = other.m_database;
        m_schema = other.m_schema;
        m_user = other.m_user;
        m_passwd = other.m_passwd;
        m_port = other.m_port;
    }
    return *this;
}

bool DbConnectionParams::isEqual(const DbConnectionParams *params) const {
    return (m_dbType == params->databaseType()) &&
            (m_server == params->serverName()) &&
            (m_database == params->databaseName()) &&
            (m_schema == params->schema()) &&
            (m_user == params->user()) &&
            (m_passwd == params->password()) &&
            (m_port == params->port());
}

void DbConnectionParams::cloneFrom(const DbConnectionParams &params) {
    setDatabaseType(params.databaseType());
    setServerName(params.serverName());
    setDatabaseName(params.databaseName());
    setSchema(params.schema());
    setUser(params.user());
    setPassword(params.password());
    setPort(params.port());
}

bool DbConnectionParams::isLocalHost() const {
    return m_server == QStringLiteral("localhost");
}

bool DbConnectionParams::isValid() const {
    return !m_dbType.isEmpty() && !m_server.isEmpty() && !m_passwd.isEmpty();
}

void DbConnectionParams::setServerName(const QString &name) {
    if (m_server != name) {
        m_server = name;
        setModified();
    }
}

void DbConnectionParams::setDatabaseName(const QString &dbname) {
    if (m_database != dbname) {
        m_database = dbname;
        setModified();
    }
}

void DbConnectionParams::setSchema(const QString &schemaname) {
    if (m_schema != schemaname) {
        m_schema = schemaname;
        setModified();
    }
}

void DbConnectionParams::setUser(const QString &username) {
    if (m_user != username) {
        m_user = username;
        setModified();
    }
}

void DbConnectionParams::setPassword(const QString &passwd) {
    if (m_passwd != passwd) {
        m_passwd = passwd;
        setModified();
    }
}

void DbConnectionParams::setDatabaseType(const QString &drivername) {
    if (m_dbType != drivername) {
        m_dbType = drivername;
        setModified();
    }
}

void DbConnectionParams::setPort(int port) {
    if (m_port != port) {
        m_port = port;
        setModified();
    }
}
