#include "DbQuery.h"
#include "DbConnection.h"
#include "LQError.h"

DbQuery::DbQuery(DbConnection *con) : QObject{con}, m_con{con} {

}

void DbQuery::prepare(const QString &str, LQError &err) {
    QString errStr;
    int db_err_code {protPrepare(str, errStr)};
    if ((db_err_code != 0) && m_repen_on_err) {
        LQError open_err;
        m_con->reopen(open_err);
        if (open_err.isSuccess()) {
            const int db_err_code2 {protPrepare(str, errStr)};
            if (db_err_code2 == 0) {
                db_err_code = 0;
            }
        }
    }

    if (db_err_code != 0) {
        err =  LQError{db_err_code,
                       tr("Cannot prepare operation '%1'! Database error: %2. Query: %3")
                       .arg(m_descr, errStr, str)};
    }
}

void DbQuery::exec(LQError &err) {
    QString errStr;
    int db_err_code {protExec(errStr)};
    if ((db_err_code != 0) && m_repen_on_err) {
        LQError open_err;
        m_con->reopen(open_err);
        if (open_err.isSuccess()) {
            const int db_err_code2 {protExec(errStr)};
            if (db_err_code2 == 0) {
                db_err_code = 0;
            }
        }
    }

    if (db_err_code != 0) {
        err =  LQError{db_err_code,
                       tr("Cannot execute operation '%1'! Database error: %2")
                       .arg(m_descr, errStr)};
    }
}

void DbQuery::exec(const QString &text, const QString &opDescr, LQError &err) {
    setOpDescription(opDescr);
    prepare(text, err);
    if (err.isSuccess())
        exec(err);
}

bool DbQuery::next() {
    return protNext();
}

void DbQuery::finish(LQError &err) {
    QString errStr;
    const int err_code {protFinish(errStr)};
    if (err_code != 0) {
        err =  LQError{err_code,
                       tr("Cannot finish operation '%1'! Database error: %2")
                       .arg(m_descr, errStr)};
    }
}

qlonglong DbQuery::getBigInt(int pos, bool *isNull) const {
    qlonglong val;
    get(pos, val, isNull);
    return val;
}

int DbQuery::getInt(int pos, bool *isNull) const {
    int val;
    get(pos, val, isNull);
    return val;
}

double DbQuery::getDouble(int pos, bool *isNull) const {
    double val;
    get(pos, val, isNull);
    return val;
}

QString DbQuery::getString(int pos, bool *isNull) const {
    QString val;
    get(pos, val, isNull);
    return val;
}

QDateTime DbQuery::getDateTime(int pos, bool *isNull) const {
    QDateTime val;
    get(pos, val, isNull);
    return val;
}

QByteArray DbQuery::getBlob(int pos, bool *isNull) const {
    QByteArray val;
    get(pos, val, isNull);
    return val;
}

void DbQuery::setOpDescription(const QString &descr) {
    m_descr = descr;
}


