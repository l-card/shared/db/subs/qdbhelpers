#ifndef DBCONNECTIONPARAMS_H
#define DBCONNECTIONPARAMS_H

#include <QString>
#include <QObject>

class DbConnectionParams : public QObject {
    Q_OBJECT
public:
    DbConnectionParams(const QString &dbType, const QString &srv, const QString &db,
                       const QString &scheme, const QString &user, const QString &passwd,
                       int port = 0);
    DbConnectionParams(const DbConnectionParams &params);
    DbConnectionParams();

    DbConnectionParams& operator=(const DbConnectionParams &other);


    const QString &serverName() const {return m_server;}
    const QString &databaseName() const {return m_database;}
    const QString &schema() const {return m_schema;}
    const QString &user() const {return m_user;}
    const QString &password() const {return m_passwd;}
    const QString &databaseType() const {return m_dbType;}
    int port() const {return m_port;}

    bool isEqual(const DbConnectionParams *params) const;
    void cloneFrom(const DbConnectionParams &params);
    bool isLocalHost() const;
    bool isValid() const;


    void setServerName(const QString &name);
    void setDatabaseName(const QString &dbname);
    void setSchema(const QString &schemaname);
    void setUser(const QString &username);
    void setPassword(const QString &passwd);
    void setDatabaseType(const QString &drivername);
    void setPort(int port);

Q_SIGNALS:
    void modified();
protected:
    void setModified() { Q_EMIT modified(); }
private:
    QString m_dbType;
    QString m_server;
    QString m_database;
    QString m_schema;
    QString m_user;
    QString m_passwd;    
    int m_port {0};
};
#endif // DBCONNECTIONPARAMS_H
