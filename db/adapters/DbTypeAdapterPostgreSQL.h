#ifndef DBTYPEADAPTERPOSTGRESQL_H
#define DBTYPEADAPTERPOSTGRESQL_H

#include "DbTypeAdapter.h"

class DbTypeAdapterPostgreSQL : public DbTypeAdapter {
    Q_OBJECT
public:
    static const DbTypeAdapterPostgreSQL &instance();

    QString typeName() const override {return QStringLiteral("PostgreSQL");}
    QString typeDisplayName() const override {return tr("PostgreSQL");}
    QString typeShortName() const override {return QStringLiteral("PostgreSQL");}

    bool supportSchema() const override {return true;}
    QString defaultSchemaName() const override {return QStringLiteral("public");}
    bool supportOSAuth() const override {return false;}
    bool supportExplPort() const override {return true;}
    unsigned defaultPort() const override {return 5432;}


    QString predefinedDatabaseName() const override {return QStringLiteral("postgres");}

    QString dbFieldType(const DbTypeDescr &type) const override;
    bool dbSameType(const DbFieldDescr &field, const DbColumnInfo &info) const override;
    QString dbFieldTimeAddSecs(const QString &field, int secs) const override;
    QString datetimeExtractPart(const QString &value, const DateTimePart &part) const override;


    QString dbSelectLimit(const QString &req, int cnt) const override;
    QString dbInsertReturnID(const QString &req, const QString &idName) const override;

    void dbLockAcquire(DbConnection &con, const DbLockInfo &lock, unsigned tout, LQError &err) const override;
    void dbLockRelease(DbConnection &con, const DbLockInfo &lock, LQError &err) const override;


    QString getDatabasesQueryString(const QString &dbname) const override;
    QString getSchemasQueryString(const QString &dbname) const override;


    void dbTableRenameColumn(DbConnection &con, const DbTableDescr &table,
                             const QString &oldName, const QString &newName, LQError &err) const override;

    void dbTableAddColumns(DbConnection &con, const QString &tableName,
                           const QList<const DbFieldDescr *> &append_fields, LQError &err) const override;

    void dbTableRemoveColumns(DbConnection &con, const QString &baseTableName,
                              const QStringList &drop_columns, LQError &err) const override;

    void dbTableChangeColumnType(DbConnection &con, const QString &tableName,
                                 const QString &column, const DbTypeDescr &type, LQError &err) const override;
    void dbTableRename(DbConnection &con,  const QString &oldName, const QString &newName, LQError &err) const override;

};

#endif // DBTYPEADAPTERPOSTGRESQL_H
