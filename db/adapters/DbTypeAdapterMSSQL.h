#ifndef DBTYPEadapterMSSQL_H
#define DBTYPEadapterMSSQL_H


#include "DbTypeAdapter.h"

class DbTypeAdapterMSSQL : public DbTypeAdapter {
    Q_OBJECT
public:
    static const DbTypeAdapterMSSQL &instance();

    QString typeName() const override {return QStringLiteral("MSSQL");}
    QString typeDisplayName() const override {return tr("Microsoft SQL Server");}
    QString typeShortName() const override {return QStringLiteral("MSSQL");}
    bool supportSchema() const override {return true;}
    QString defaultSchemaName() const override {return QStringLiteral("dbo");}
    bool supportOSAuth() const override {return true;}
    bool supportExplPort() const override {return false;}
    unsigned defaultPort() const override {return 1433;}

    QString predefinedDatabaseName() const override {return QStringLiteral("tempdb");}

    QString boolValText(bool val) const override {return val ? QStringLiteral("1") : QStringLiteral("0");}

    QString dbFieldType(const DbTypeDescr &type) const override;
    QString dbFieldTimeAddSecs(const QString &field, int secs) const override;
    QString datetimeExtractPart(const QString &value, const DateTimePart &part) const override;

    QString dbSelectLimit(const QString &req, int cnt) const override;
    QString dbGetLastIDQueryText() const override;


    void dbLockAcquire(DbConnection &con, const DbLockInfo &lock, unsigned tout, LQError &err) const override;
    void dbLockRelease(DbConnection &con, const DbLockInfo &lock, LQError &err) const override;


    QString getDatabasesQueryString(const QString &dbname) const override;
    QString getSchemasQueryString(const QString &dbname) const override;
    void dbTableRenameColumn(DbConnection &con, const DbTableDescr &table,
                             const QString &oldName, const QString &newName, LQError &err) const override;
    void dbTableChangeColumnType(DbConnection &con, const QString &tableName,
                                 const QString &column, const DbTypeDescr &type, LQError &err) const override;
    void dbTableRename(DbConnection &con, const QString &oldName, const QString &newName, LQError &err) const override;

    static const unsigned connection_tout {3};

};




#endif // DBTYPEadapterMSSQL_H
