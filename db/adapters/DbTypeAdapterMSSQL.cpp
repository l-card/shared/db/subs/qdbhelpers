#include "DbTypeAdapterMSSQL.h"
#include "db/DbQuery.h"
#include "db/DbConnection.h"
#include "db/DbLockInfo.h"
#include "db/tabledescr/DbTableDescr.h"
#include "db/tabledescr/DbTypeDescr.h"
#include <QElapsedTimer>
#include <QStringBuilder>
#include <memory>

const DbTypeAdapterMSSQL &DbTypeAdapterMSSQL::instance() {
    static const DbTypeAdapterMSSQL adapter;
    return adapter;
}

QString DbTypeAdapterMSSQL::dbFieldType(const DbTypeDescr &type) const {
    switch (type.type()) {
        case DbTypeDescr::Type::BigInteger:
            return QStringLiteral("BIGINT");
        case DbTypeDescr::Type::Integer:
            return QStringLiteral("INT");
        case DbTypeDescr::Type::SmallInteger:
            return QStringLiteral("SMALLINT");
        case DbTypeDescr::Type::Float:
            return QStringLiteral("REAL");
        case DbTypeDescr::Type::Double:
            return QStringLiteral("FLOAT");
        case DbTypeDescr::Type::String:
            return QStringLiteral("NVARCHAR") % QString("(%1)").arg(type.len() < 0 ? 512 : type.len());
        case DbTypeDescr::Type::LongString:
        case DbTypeDescr::Type::LongText:
            return QStringLiteral("NVARCHAR(max)");
        case DbTypeDescr::Type::DateTime:
            return QStringLiteral("DATETIME2") % (type.len() < 0 ? QString() : QString("(%1)").arg(type.len()));
        case DbTypeDescr::Type::Date:
            return QStringLiteral("Date");
        case DbTypeDescr::Type::Time:
            return QStringLiteral("TIME") % (type.len() < 0 ? QString() : QString("(%1)").arg(type.len()));
        case DbTypeDescr::Type::Image:
        case DbTypeDescr::Type::Blob:
            return QStringLiteral("VARBINARY(MAX)");
        case DbTypeDescr::Type::Color:
            return QStringLiteral("INT");
        case DbTypeDescr::Type::Bool:
            return QStringLiteral("BIT");
        case DbTypeDescr::Type::AutoID:
            return QStringLiteral("BIGINT IDENTITY(1,1)");
        default:
            return QString();
    }
}

QString DbTypeAdapterMSSQL::dbFieldTimeAddSecs(const QString &field, int secs) const {
    return QString("DATEADD(ss,%1,%2)").arg(secs).arg(field);
}

QString DbTypeAdapterMSSQL::datetimeExtractPart(const QString &value, const DateTimePart &part) const {
    /* https://docs.microsoft.com/ru-ru/sql/t-sql/functions/datepart-transact-sql?view=sql-server-2017 */
    QString partText;
    switch(part) {
        case DateTimePart_Hour: partText   = QStringLiteral("hour"); break;
        case DateTimePart_Minute: partText = QStringLiteral("minute"); break;
        case DateTimePart_Second: partText = QStringLiteral("second"); break;
        case DateTimePart_Year: partText   = QStringLiteral("year"); break;
        case DateTimePart_Quarter: partText= QStringLiteral("quarter"); break;
        case DateTimePart_Month: partText  = QStringLiteral("month"); break;
        case DateTimePart_Week: partText   = QStringLiteral("ISO_WEEK"); break;
        case DateTimePart_Day: partText    = QStringLiteral("day"); break;
    }
    return QString("DATEPART(%1, %2)").arg(partText, value);
}

QString DbTypeAdapterMSSQL::dbSelectLimit(const QString &req, int cnt) const {
    const int pos {req.indexOf(QLatin1String("SELECT")) + 6};
    return QString(req).insert(pos, QString(" TOP %1 ").arg(cnt));
}

QString DbTypeAdapterMSSQL::dbGetLastIDQueryText() const {
    return QStringLiteral("SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];");
}

void DbTypeAdapterMSSQL::dbLockAcquire(DbConnection &con, const DbLockInfo &lock, unsigned tout, LQError &err) const {
    QElapsedTimer tmr;
    tmr.start();
    /* Так как результат процедуры не возвращается и не очень понятно как его получить
     * через QSqlQuery, а выход по таймауту не приводит к ошибке запроса,
     * то узнать, что вышли из блокировки по таймауту можно косвенно
     * по времени выполнения операции. */
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(QString("EXEC sp_getapplock '%1', 'Exclusive', 'Session', '%2'")
                .arg(lock.name()).arg(tout),
                tr("Lock %1 acquire").arg(lock.name()), err);
    if (err.isSuccess()) {
        qint64 elapsed = tmr.elapsed();
        if (elapsed > tout) {
            err = LQError(-1, tr("Lock %1 acquire timeout").arg(lock.name()));
        }
    }
}

void DbTypeAdapterMSSQL::dbLockRelease(DbConnection &con, const DbLockInfo &lock, LQError &err) const {
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(QString("EXEC sp_releaseapplock '%1', 'Session'").arg(lock.name()),
                tr("Lock %1 release").arg(lock.name()), err);
}

QString DbTypeAdapterMSSQL::getDatabasesQueryString(const QString &dbname) const {
    return QString("SELECT name FROM sys.databases WHERE sys.databases.name = '%1'").arg(dbname);
}

QString DbTypeAdapterMSSQL::getSchemasQueryString(const QString &dbname) const {
    return QString("SELECT name FROM sys.schemas WHERE sys.schemas.name = '%1'").arg(dbname);
}


void DbTypeAdapterMSSQL::dbTableRenameColumn(DbConnection &con, const DbTableDescr &table,
                                             const QString &oldName,  const QString &newName, LQError &err) const {
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    const QString qtext {QString("EXEC sp_RENAME '%1.%2' , '%3', 'COLUMN'")
            .arg(con.dbTableFullName(table.name()), oldName, newName)};
    query->exec(qtext, tr("Column %1 rename from table %2").arg(oldName, table.name()), err);
}

void DbTypeAdapterMSSQL::dbTableChangeColumnType(DbConnection &con, const QString &tableName,
                                                 const QString &column, const DbTypeDescr &type, LQError &err) const {
    const QString queryText {QString("ALTER TABLE %0 ALTER COLUMN %1 %2;")
            .arg(con.dbTableFullName(tableName),
                column,
                dbFieldType(type))};
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(queryText, tr("Change column %2 type of table %1")
                .arg(tableName, column), err);
}

void DbTypeAdapterMSSQL::dbTableRename(DbConnection &con, const QString &oldName, const QString &newName, LQError &err) const {
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    const QString qtext {QString("EXEC sp_RENAME '%1' , '%2'")
                .arg(con.dbTableFullName(oldName), newName)};
    query->exec(qtext, tr("Table %1 rename to %2").arg(oldName, newName), err);
}
