#include "DbTypeAdapterPostgreSQL.h"
#include "db/tabledescr/DbTableDescr.h"
#include "db/tabledescr/DbFieldDescr.h"
#include "db/tabledescr/DbTypeDescr.h"
#include "db/DbColumnInfo.h"
#include "db/DbLockInfo.h"
#include "db/DbConnection.h"
#include "db/DbQuery.h"
#include <QStringBuilder>
#include <memory>

const DbTypeAdapterPostgreSQL &DbTypeAdapterPostgreSQL::instance() {
    static const DbTypeAdapterPostgreSQL adapter;
    return adapter;
}

QString DbTypeAdapterPostgreSQL::dbFieldType(const DbTypeDescr &type) const {
    switch (type.type()) {
        case DbTypeDescr::Type::BigInteger:
            return QStringLiteral("bigint");
        case DbTypeDescr::Type::Integer:
            return QStringLiteral("integer");
        case DbTypeDescr::Type::SmallInteger:
            return QStringLiteral("smallint");
        case DbTypeDescr::Type::Float:
            return QStringLiteral("real");
        case DbTypeDescr::Type::Double:
            return QStringLiteral("double precision");
        case DbTypeDescr::Type::String:
            return QString("character varying(%1)").arg(type.len() < 0 ? 512 : type.len());
        case DbTypeDescr::Type::LongString:
            return QStringLiteral("character varying");
        case DbTypeDescr::Type::LongText:
            return QStringLiteral("text");
        case DbTypeDescr::Type::DateTime:
            return QStringLiteral("timestamp") % (type.len() < 0 ? QString() : QString("(%1)").arg(type.len()));
        case DbTypeDescr::Type::Date:
            return QStringLiteral("date");
        case DbTypeDescr::Type::Time:
            return QStringLiteral("time") % (type.len() < 0 ? QString() : QString("(%1)").arg(type.len()));
        case DbTypeDescr::Type::Blob:
            return QStringLiteral("bytea") + (type.len() < 0 ? QString() : QString("(%1)").arg(type.len()));
        case DbTypeDescr::Type::Image:
            return QStringLiteral("bytea");
        case DbTypeDescr::Type::Color:
            return QStringLiteral("integer");
        case DbTypeDescr::Type::Bool:
            return QStringLiteral("boolean");
        case DbTypeDescr::Type::AutoID:
            return QStringLiteral("bigserial");
        default:
            return QString{};
    }
}

bool DbTypeAdapterPostgreSQL::dbSameType(const DbFieldDescr &field, const DbColumnInfo &info) const {
    bool ret {false};
    if (field.type().type() == DbTypeDescr::Type::AutoID) {
        if ((info.type().toLower() == QLatin1String("bigint")) &&
                (info.extra().toLower().contains(QLatin1String("auto_increment")))) {
            ret = true;
        }
    } else if ((field.type().type() == DbTypeDescr::Type::Bool) &&
               (info.type().toLower() == QLatin1String("tinyint"))) {
        /* boolean и tinyint эквиваленты в mysql/mariadb */
        ret = true;
    } else {
        ret = DbTypeAdapter::dbSameType(field, info);
    }
    return ret;
}

QString DbTypeAdapterPostgreSQL::dbFieldTimeAddSecs(const QString &field, int secs) const {
    return QString("DATE_ADD(%1, INTERVAL %2 SECOND)").arg(field).arg(secs);
}

QString DbTypeAdapterPostgreSQL::datetimeExtractPart(const QString &value, const DateTimePart &part) const {
    /* https://mariadb.com/kb/en/library/extract/ */
    QString partText;
    switch(part) {
        case DateTimePart_Hour: partText    = QStringLiteral("hour"); break;
        case DateTimePart_Minute: partText  = QStringLiteral("minute"); break;
        case DateTimePart_Second: partText  = QStringLiteral("second"); break;
        case DateTimePart_Year: partText    = QStringLiteral("year"); break;
        case DateTimePart_Quarter: partText = QStringLiteral("quarter"); break;
        case DateTimePart_Month: partText   = QStringLiteral("month"); break;
        case DateTimePart_Week: partText    = QStringLiteral("week"); break;
        case DateTimePart_Day: partText     = QStringLiteral("day"); break;
    }
    return QString("EXTRACT %1 FROM %2").arg(partText, value);
}

QString DbTypeAdapterPostgreSQL::dbSelectLimit(const QString &req, int cnt) const {
    return req % QString(" LIMIT %1").arg(cnt);
}

QString DbTypeAdapterPostgreSQL::dbInsertReturnID(const QString &req, const QString &idName) const {
    QString ret = req;
    if (ret.endsWith(';'))
        ret.remove(ret.length()-1,1);
    return ret.append(QString(" RETURNING %1;").arg(idName));
}

void DbTypeAdapterPostgreSQL::dbLockAcquire(DbConnection &con, const DbLockInfo &lock, unsigned tout, LQError &err) const {
    /** @todo set tout */
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(QString("SELECT pg_advisory_lock(%1);").arg(lock.id()),
                tr("Lock %1 acquire").arg(lock.name()), err);
}

void DbTypeAdapterPostgreSQL::dbLockRelease(DbConnection &con, const DbLockInfo &lock, LQError &err) const {
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(QString("SELECT pg_advisory_unlock(%1);").arg(lock.id()),
                tr("Lock %1 release").arg(lock.name()), err);
}

QString DbTypeAdapterPostgreSQL::getDatabasesQueryString(const QString &dbname) const {
    return QString("SELECT datname FROM pg_database WHERE datname = '%1'").arg(dbname);
}

QString DbTypeAdapterPostgreSQL::getSchemasQueryString(const QString &name) const {
    return QString("SELECT nspname FROM pg_namespace WHERE nspname = '%1'").arg(name);
}


void DbTypeAdapterPostgreSQL::dbTableRenameColumn(DbConnection &con, const DbTableDescr &table,
                                                     const QString &oldName, const QString &newName, LQError &err) const {
    const QString qtext {QString("ALTER TABLE %1 RENAME COLUMN %2 TO %3")
            .arg(con.dbTableFullName(table.name()),
                 oldName,
                 dbCreateColumnText(con.connectionParams(), table.fieldDescr(newName)))};
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(qtext,  tr("Column %1 rename").arg(oldName), err);
}

void DbTypeAdapterPostgreSQL::dbTableAddColumns(DbConnection &con, const QString &tableName,
                                                   const QList<const DbFieldDescr *> &append_fields, LQError &err) const {

    for (const DbFieldDescr *field : append_fields) {
        QString queryText = QString("ALTER TABLE %0 ADD COLUMN %1;")
                .arg(con.dbTableFullName(tableName),
                     dbCreateColumnText(con.connectionParams(), field));
        const std::unique_ptr<DbQuery> query{con.createQuery()};
        query->exec(queryText, tr("Addition column to table %1")
                    .arg(con.dbTableFullName(tableName)), err);
        if (!err.isSuccess())
            break;
    }
}

void DbTypeAdapterPostgreSQL::dbTableRemoveColumns(DbConnection &con, const QString &tableName,
                                                   const QStringList &drop_columns, LQError &err) const {
    for (const QString &column : drop_columns) {
        if (err.isSuccess())
            dbTableRemoveColumnConstrains(con, tableName, column, err);
        if (err.isSuccess()) {
            QString queryText {QString("ALTER TABLE %0 DROP COLUMN %1 CASCADE;")
                                .arg(con.dbTableFullName(tableName), column)};
            const std::unique_ptr<DbQuery> query{con.createQuery()};
            query->exec(queryText, tr("Removal column %2 from table %1")
                        .arg(tableName, column), err);
        }
    }
}

void DbTypeAdapterPostgreSQL::dbTableChangeColumnType(DbConnection &con, const QString &tableName,
                                                      const QString &column, const DbTypeDescr &type, LQError &err) const {
    QString typeStr {dbFieldType(type)};

    if (type.type() == DbTypeDescr::Type::AutoID) {
        typeStr = QStringLiteral("bigint");
    }

    QString queryText {QString("ALTER TABLE %0 ALTER COLUMN %1 SET DATA TYPE %2;")
            .arg(con.dbTableFullName(tableName),
                column,
                typeStr)};

    if (type.type() == DbTypeDescr::Type::AutoID) {
        QString seqName = QString("%0.%1_%2_seq").arg(con.connectionParams().schema(), tableName, column);
        queryText.append(QString("CREATE SEQUENCE %1;").arg(seqName));
        queryText.append(QString("ALTER TABLE %1 ALTER COLUMN %2 SET DEFAULT nextval('%3');")
                         .arg(con.dbTableFullName(tableName),
                              column,
                              seqName));
        queryText.append(QString("ALTER TABLE %1 ALTER COLUMN %2 SET NOT NULL;")
                         .arg(con.dbTableFullName(tableName),
                              column));
        queryText.append(QString("ALTER SEQUENCE %3 OWNED BY %1.%2;")
                         .arg(con.dbTableFullName(tableName),
                              column,
                              seqName));
        queryText.append(QString("SELECT setval('%3', (SELECT MAX(%2) FROM %1));")
                         .arg(con.dbTableFullName(tableName),
                              column,
                              seqName));
    }

    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(queryText, tr("Change column %2 type of table %1")
                .arg(tableName, column), err);
}

void DbTypeAdapterPostgreSQL::dbTableRename(DbConnection &con, const QString &oldName, const QString &newName, LQError &err) const {
    const QString queryText {QString("ALTER TABLE %1 RENAME TO '%2'")
                .arg(oldName, newName)};
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(queryText, tr("Rename table from %1 to %2")
                .arg(oldName, newName), err);
}

