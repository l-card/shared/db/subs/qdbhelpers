#ifndef DBTYPEadapterMYSQL_H
#define DBTYPEadapterMYSQL_H

#include "DbTypeAdapter.h"


class DbTypeAdapterMySQL : public DbTypeAdapter {
    Q_OBJECT
public:
    static const DbTypeAdapterMySQL &instance();

    QString typeName() const override {return QStringLiteral("MySQL");}
    QString typeDisplayName() const override {return tr("MariaDB/MySQL");}
    QString typeShortName() const override {return QStringLiteral("MariaDB");}

    bool supportSchema() const override {return false;}
    QString defaultSchemaName() const override {return QString();}
    bool supportOSAuth() const override {return false;}
    bool supportExplPort() const override {return true;}
    unsigned defaultPort() const override {return 3306;}

    QString predefinedDatabaseName() const override {return QStringLiteral("information_schema");}

    QString dbFieldType(const DbTypeDescr &type) const override;
    bool dbSameType(const DbFieldDescr &field, const DbColumnInfo &info) const override;
    QString dbFieldTimeAddSecs(const QString &field, int secs) const override;
    QString datetimeExtractPart(const QString &value, const DateTimePart &part) const override;

    QString dbSelectLimit(const QString &req, int cnt) const override;
    QString dbGetLastIDQueryText() const override;


    void dbLockAcquire(DbConnection &con, const DbLockInfo &lock, unsigned tout, LQError &err) const override;
    void dbLockRelease(DbConnection &con, const DbLockInfo &lock, LQError &err) const override;


    QString getDatabasesQueryString(const QString &dbname) const override;
    QString getSchemasQueryString(const QString &dbname) const override {return QString();}


    void dbTableRenameColumn(DbConnection &con, const DbTableDescr &table,
                             const QString &oldName, const QString &newName, LQError &err) const override;

    void dbTableAddColumns(DbConnection &con, const QString &baseTableName,
                           const QList<const DbFieldDescr *> &append_fields, LQError &err) const override;

    void dbTableRemoveColumns(DbConnection &con, const QString &baseTableName,
                              const QStringList &drop_columns, LQError &err) const override;

    void dbTableChangeColumnType(DbConnection &con, const QString &tableName,
                                    const QString &column, const DbTypeDescr &type, LQError &err) const override;
    void dbTableRename(DbConnection &con,  const QString &oldName, const QString &newName, LQError &err) const override;
};



#endif // DBTYPEadapterMYSQL_H
