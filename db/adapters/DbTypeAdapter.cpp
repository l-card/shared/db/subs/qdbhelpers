#include "DbTypeAdapter.h"
#include "db/DbConnectionParams.h"
#include "db/DbConnection.h"
#include "db/DbQuery.h"
#include "db/tabledescr/DbTableDescr.h"
#include "db/tabledescr/DbFieldDescr.h"
#include "db/tabledescr/DbFKeyDescr.h"
#include "db/tabledescr/DbIndexDescr.h"
#include "db/DbColumnInfo.h"
#include "db/DbLockInfo.h"
#include <QStringBuilder>
#include <memory>

struct ColumnDescr {
    ColumnDescr(const QString &name_, const QString &type_) : name(name_), type(type_) {}

    QString name;
    QString type;
};


bool DbTypeAdapter::dbSameType(const DbFieldDescr &field, const DbColumnInfo &info) const {
    const QString dbType {dbFieldType(field.type()).toLower().split('(').at(0)};
    return info.type().toLower().startsWith(dbType);
}

bool DbTypeAdapter::dbHasTable(const QString &tableName, const QStringList &existedTables) const {
    for (const QString &checkTableName : existedTables) {
        if (tableName.toLower() == checkTableName.toLower())
            return true;
    }
    return false;
}

QString DbTypeAdapter::dbTableFullName(const DbConnectionParams &params, const QString &tableName) const {
    QString ret;
    if (supportSchema()) {
        ret = params.schema() % QLatin1Char('.');
    }
    ret += tableName;
    return ret;
}

QString DbTypeAdapter::dbInformationViewSchemeName(const DbConnectionParams &params) const {
    return supportSchema() ? params.schema() : params.databaseName();
}

void DbTypeAdapter::dbGetTableNames(DbConnection &con, QStringList &tableNames, LQError &err) const {
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    tableNames.clear();
    const QString qstr {QString("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '%1'").
                arg(dbInformationViewSchemeName(con.connectionParams()))};
    /* получаем список всех созданных таблиц */
    query->exec(qstr,  tr("Obtaining database table list"), err);
    if (err.isSuccess()) {
        while (query->next()) {
             tableNames.append(query->getString(0));
        }
    }
}

void DbTypeAdapter::dbSyncTable(DbConnection &con, const DbTableDescr &table, const QStringList &existedTables, LQError &err) const {
    /* если таблицы не существовало - создаем новую */
    if (!dbHasTable(table.name(), existedTables)) {
        dbCreateTable(con, table, CreateTableParams{}, err);
    } else {
        /* проверяем список столбоцов */
        const std::unique_ptr<DbQuery> query(con.createQuery());
        const QList<const DbFieldDescr *> &fields {table.fields()};
        QList<const DbFieldDescr *> append_fields;
        QList<const DbFieldDescr *> type_change_fields;

        QString queryText = QString("SELECT %1 FROM INFORMATION_SCHEMA.COLUMNS"
                                    " WHERE LOWER(%2) = LOWER('%4') AND LOWER(%3)=LOWER('%5');")
                .arg(DbColumnInfo::fieldNamesString(),
                     DbColumnInfo::fieldNameTableName(),
                     DbColumnInfo::fieldNameSchema(),
                     table.name(),
                     dbInformationViewSchemeName(con.connectionParams()));
        query->exec(queryText,  tr("Obtaining column list of table %1")
                    .arg(con.dbTableFullName(table.name())), err);

        if (err.isSuccess()) {
            QList<const DbColumnInfo *> columns;
            QStringList drop_columns;
            while (query->next()) {
                columns.append(new DbColumnInfo{*query});
            }

            for (const DbColumnInfo *column : qAsConst(columns)) {
                bool fnd {false};
                for (int f {0}; (f < fields.size()) && !fnd; ++f) {
                    const DbFieldDescr *field {fields.at(f)};
                    if (field->sameName(column->name())) {
                        fnd = true;
                        if (!dbSameType(*field, *column)) {
                            type_change_fields.append(field);
                        }
                    }
                }
                if (!fnd) {
                    drop_columns.append(column->name());
                }
            }

            for (const DbFieldDescr *field : qAsConst(fields)) {
                bool fnd {false};
                for (int col {0}; (col < columns.size()) && !fnd; ++col) {
                    if (field->sameName(columns.at(col)->name())) {
                        fnd = true;
                    }
                }

                if (!fnd) {
                    append_fields.append(field);
                }
            }

            qDeleteAll(columns);
            columns.clear();


            /* удаление лишних столбцов */
            if (!drop_columns.isEmpty()) {
                dbTableRemoveColumns(con, table.name(), drop_columns, err);
            }

            /* добавление новых столбцов */
            if (err.isSuccess() && !append_fields.isEmpty()) {
                dbTableAddColumns(con, table.name(), append_fields, err);
            }

            if (err.isSuccess() && !type_change_fields.isEmpty()) {
                for (const DbFieldDescr *field : qAsConst(type_change_fields)) {
                    if (err.isSuccess())
                        dbTableChangeColumnType(con, table.name(), field->fieldName(), field->type(), err);
                }
            }
        }
    }

    /* проверяем, нужны ли в таблице индексы */
    if (err.isSuccess()) {
        const QList<const DbIndexDescr *> &indexes {table.indexes()};

        if (!indexes.isEmpty()) {
#if 0
            QSqlQuery query(sqlDb);
            query.setForwardOnly(true);
            /* получаем список индексов */
            if (!query.exec(QString("SELECT IndexName = ind.name, ColumnName = col.name"
                                    " FROM sys.indexes ind"
                                    " INNER JOIN sys.index_columns ic ON  ind.object_id = ic.object_id and ind.index_id = ic.index_id"
                                    " INNER JOIN sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id"
                                    " INNER JOIN sys.tables t ON ind.object_id = t.object_id"
                                    " INNER JOIN sys.schemas sh ON t.schema_id = sh.schema_id"
                                    " WHERE ind.is_primary_key = 0"
                                    " AND ind.is_unique = 0"
                                    " AND ind.is_unique_constraint = 0"
                                    " AND t.is_ms_shipped = 0"
                                    " AND t.name = '%1' AND sh.name = '%2';")
                            .arg(descr->baseTableName()).arg(schema()))) {
                err = LQError::error(tr("Obtaining index list from table %1").
                                     arg(descr->tableName(schema())), query.lastError());
            } else {
                while (query.next()) {
                    QString idxName = query.value(0).toString();
                    for (int i=0; i < indexes.size(); i++) {
                        if (indexes.at(i)->name() == idxName)  {
                            indexes.removeAt(i);
                            break;
                        }
                    }
                }

                /* создаем все индексы, которых не нашли */
                if (indexes.size() != 0)  {
                    for (int i=0;(i < indexes.size()); i++) {
                        QSqlQuery idxQuery(sqlDb());
                        QString fieldStr;
                        const DbIndexDescr *idxDescr = indexes.at(i);
                        int fields_cnt = idxDescr->fields().size();
                        for (int i = 0; i < fields_cnt; i++) {
                            fieldStr += idxDescr->fields().at(i);
                            if (i != (fields_cnt - 1))
                                fieldStr += ", ";
                        }

                        if (!idxQuery.exec(QString("CREATE %1 INDEX %2 ON %3 (%4);")
                                           .arg(idxDescr->type(),
                                                idxDescr->name(),
                                                descr->tableName(schema()),
                                                fieldStr))) {
                            if (err.isSuccess()) {
                                err = LQError::error(tr("Create index %1 in table %2").
                                                     .arg(idxDescr->name()).arg(descr->tableName(schema())), query.lastError());
                            }
                        }
                    }
                }
            }
#endif
        }
    }
}

void DbTypeAdapter::dbCreateTable(DbConnection &con, const DbTableDescr &table,
                                  const CreateTableParams &opts, LQError &err) const {
    QString queryText {QString("CREATE TABLE %1 (")
                .arg(con.dbTableFullName(table.name()))};
    const QList<const DbFieldDescr *> &fields {table.fields()};

    for (int f {0}; f < fields.size(); ++f) {
        if (f != 0)
            queryText.append(QLatin1String(", "));
        queryText.append(dbCreateColumnText(con.connectionParams(), fields.at(f)));
    }

    if (!opts.noFk) {
        const QList<const DbFKeyDescr *> &fks {table.foreignKeys()};
        for (const DbFKeyDescr *fk : fks) {
            queryText += QString(", CONSTRAINT %0 FOREIGN KEY (%1) REFERENCES %2(%3) %4 ")
                    .arg(fk->name(),
                        fk->fieldName(),
                        con.dbTableFullName(fk->refTableName()),
                        fk->refFieldName(),
                        fk->action());
        }
    }

    queryText.append(QLatin1String(");"));


    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(queryText, tr("Table creation %1")
                .arg(con.dbTableFullName(table.name())), err);

    if (err.isSuccess() && !opts.noIdx) {
        const QList<const DbIndexDescr *> &indexes{table.indexes()};
        for(const DbIndexDescr *index : indexes) {
            if (err.isSuccess()) {
                dbCreateIdx(con, table.name(), *index, err);
            }
        }
    }
}

void DbTypeAdapter::dbDropTable(DbConnection &con, const QString &tableName, LQError &err) const {
    const QString queryText {QString("DROP TABLE %1;").arg(con.dbTableFullName(tableName))};
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(queryText, tr("Table deletion %1")
                .arg(con.dbTableFullName(tableName)), err);
}



QString DbTypeAdapter::dbCreateColumnText(const DbConnectionParams &params, const DbFieldDescr *field) const {
    QString ret {field->fieldName() % QLatin1Char(' ') % dbFieldType(field->type())};
    if (!field->defaultValue().isEmpty())
        ret += QString(" DEFAULT %1").arg(field->defaultValue());
    ret += field->isNullable() ? QStringLiteral(" NULL") : QStringLiteral(" NOT NULL");
    if (!field->dbConstrains().isEmpty())
        ret += QLatin1Char(' ') % field->dbConstrains();
    return ret;
}

void DbTypeAdapter::dbTableAddColumns(DbConnection &con,  const QString &baseTableName,
                                      const QList<const DbFieldDescr *> &append_fields, LQError &err) const {
    QString queryText {QString("ALTER TABLE %0 ADD ").arg(con.dbTableFullName(baseTableName))};

    const std::unique_ptr<DbQuery> query{con.createQuery()};
    for (int f {0}; f < append_fields.size(); ++f) {
        queryText += dbCreateColumnText(con.connectionParams(), append_fields.at(f));
        if (f != (append_fields.size()-1)) {
            queryText += QLatin1String(", ");
        } else {
            queryText += QLatin1String(";");
        }
    }

    query->exec(queryText, tr("Addition column to table %1").
                arg(con.dbTableFullName(baseTableName)), err);
}

void DbTypeAdapter::dbTableRemoveColumns(DbConnection &con, const QString &baseTableName,
                                            const QStringList &drop_columns, LQError &err) const {
    QString queryText {QString("ALTER TABLE %0 DROP COLUMN ")
                .arg(con.dbTableFullName(baseTableName))};

    for (int col {0}; (col < drop_columns.size()) && err.isSuccess(); ++col) {
        dbTableRemoveColumnConstrains(con, baseTableName, drop_columns.at(col), err);
        if (err.isSuccess()) {
            queryText.append(drop_columns.at(col));
            if (col != (drop_columns.size()-1)) {
                queryText += QLatin1String(", ");
            } else {
                queryText += QLatin1String(";");
            }
        }
    }

    if (err.isSuccess()) {
        const std::unique_ptr<DbQuery> query{con.createQuery()};
        query->exec(queryText, tr("Columns removal from table %1"), err);
    }
}

void DbTypeAdapter::dbTableRemoveColumnConstrains(DbConnection &con, const QString &baseTableName,
                                                  const QString &columnName, LQError &err) const {
    QStringList constrNames;
    QString fullTableName = con.dbTableFullName(baseTableName);
    QString qtext{QString("SELECT KCU.CONSTRAINT_NAME FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC \
                        INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU \
                        ON KCU.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG   \
                        AND KCU.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA  \
                        AND KCU.CONSTRAINT_NAME = RC.CONSTRAINT_NAME \
                    WHERE \
                        KCU.TABLE_SCHEMA = '%0' AND \
                        KCU.TABLE_NAME = '%1' AND  \
                        KCU.COLUMN_NAME = '%2';")
                .arg(dbInformationViewSchemeName(con.connectionParams()), baseTableName, columnName)};
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(qtext, tr("Obtaining column %2 of table %1 constrains")
                .arg(fullTableName, columnName), err);
    if (err.isSuccess()) {
        while (query->next()) {
            constrNames << query->getString(0);
        }
        query->finish(err);

        for (const QString &conName : qAsConst(constrNames)) {
            qtext = QString("ALTER TABLE %1 DROP CONSTRAINT %2;")
                    .arg(fullTableName, conName);
            LQError remErr;
            query->exec(qtext, tr("Removal constrain %2 from table %1")
                        .arg(fullTableName, conName), remErr);
            err += remErr;
        }
    }
}

void DbTypeAdapter::dbCreateFK(DbConnection &con, const QString &tableName, const DbFKeyDescr &fk, LQError &err) const {
    const QString queryStr {
            QString("ALTER TABLE %1 ADD CONSTRAINT %2 FOREIGN KEY (%3) REFERENCES %4(%5) %6;")
                    .arg(con.dbTableFullName(tableName),
                        fk.name(),
                        fk.fieldName(),
                        fk.refTableName(),
                        fk.refFieldName(),
                        fk.action())
    };
    const std::unique_ptr<DbQuery> query{con.createQuery()};
     query->exec(queryStr, tr("Create foreign key %1").arg(fk.name()), err);
}

void DbTypeAdapter::dbDropFK(DbConnection &con, const QString &tableName, const QString &fkName, LQError &err) const {
    const QString queryStr {QString("ALTER TABLE %1 DROP CONSTRAINT %2;")
                .arg(con.dbTableFullName(tableName), fkName)};

    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(queryStr, tr("Drop foreign key %1").arg(fkName), err);
}

void DbTypeAdapter::dbCreateIdx(DbConnection &con, const QString &tableName, const DbIndexDescr &dbIndex, LQError &err) const {
    QString fieldStr;
    const int fields_cnt {dbIndex.fields().size()};
    for (int i {0}; i < fields_cnt; ++i) {
        fieldStr += dbIndex.fields().at(i);
        if (i != (fields_cnt - 1))
            fieldStr += QLatin1String(", ");
    }

    const QString queryStr {QString("CREATE %1 INDEX %2 ON %3 (%4);")
            .arg(dbIndex.type(),
                 dbIndex.name(),
                 con.dbTableFullName(tableName),
                 fieldStr)};
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(queryStr, tr("Create index %1 on table %2").arg(dbIndex.name(), tableName), err);
}
