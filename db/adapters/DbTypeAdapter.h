#ifndef DBTYPEadapter_H
#define DBTYPEadapter_H

#include <QObject>
#include "LQError.h"

class DbConnection;
class DbConnectionParams;
class DbTableDescr;
class DbFieldDescr;
class DbTypeDescr;
class DbFKeyDescr;
class DbIndexDescr;
class DbColumnInfo;
class DbLockInfo;

class DbTypeAdapter : public QObject {
    Q_OBJECT
public:
    struct CreateTableParams {
        CreateTableParams() : noFk(false), noIdx(false) {}
        bool noFk;
        bool noIdx;
    };

    enum DateTimePart {
        DateTimePart_Hour,
        DateTimePart_Minute,
        DateTimePart_Second,
        DateTimePart_Year,
        DateTimePart_Quarter,
        DateTimePart_Month,
        DateTimePart_Week,
        DateTimePart_Day
    };

    virtual QString typeName() const = 0;
    virtual QString typeDisplayName() const = 0;
    virtual QString typeShortName() const = 0;

    virtual bool supportSchema() const = 0; /* поддержка разделения на схемы внутри базы данных */
    virtual QString defaultSchemaName() const = 0;
    virtual bool supportOSAuth() const = 0; /* поддержка аутентификации через пользователя ОС */
    virtual bool supportExplPort() const  = 0; /* поддержка явного указания порта при подключении (не через имя сервера) */
    virtual unsigned defaultPort() const = 0;

    virtual QString predefinedDatabaseName() const = 0;  /* название базы данных, которая всегда пристутсвует для данного типа сервера */


    virtual QString timeStringFormat() const {return QStringLiteral("HH:mm:ss");}
    virtual QString dateStringFormat() const {return QStringLiteral("yyyy-MM-dd");}
    virtual QString dateTimeStringFormat() const {return QStringLiteral("yyyy-MM-ddTHH:mm:ss");}
    virtual QString boolValText(bool val) const {return val ? QStringLiteral("true") : QStringLiteral("false");}


    virtual QString dbFieldType(const DbTypeDescr& type) const = 0;
    virtual bool dbSameType(const DbFieldDescr &field, const DbColumnInfo &info) const;
    virtual QString dbFieldTimeAddSecs(const QString &field, int secs) const = 0;
    virtual QString datetimeExtractPart(const QString &value, const DateTimePart &part) const = 0;


    virtual QString dbSelectLimit(const QString &req, int cnt) const = 0;
    virtual QString dbInsertReturnID(const QString &req, const QString &idName) const {return req;}
    virtual QString dbGetLastIDQueryText() const {return QString();}


    virtual void dbLockAcquire(DbConnection &con, const DbLockInfo &lock, unsigned tout, LQError &err) const = 0;
    virtual void dbLockRelease(DbConnection &con, const DbLockInfo &lock, LQError &err) const = 0;


    virtual QString getDatabasesQueryString(const QString &dbname) const = 0;
    virtual QString getSchemasQueryString(const QString &dbname) const = 0;


    virtual bool dbHasTable(const QString &tableName, const QStringList &existedTables) const;
    virtual QString dbTableFullName(const DbConnectionParams &params, const QString &baseTableName) const;
    virtual QString dbInformationViewSchemeName(const DbConnectionParams &params) const;
    virtual void dbGetTableNames(DbConnection &con, QStringList &tableNames, LQError &err) const;
    virtual void dbSyncTable(DbConnection &con, const DbTableDescr &table, const QStringList &existedTables, LQError &err) const;
    virtual void dbCreateTable(DbConnection &con, const DbTableDescr &table, const CreateTableParams &opts, LQError &err) const;
    virtual void dbDropTable(DbConnection &con, const QString &tableName, LQError &err) const;

    virtual QString dbCreateColumnText(const DbConnectionParams &params, const DbFieldDescr *field) const;
    virtual void dbTableAddColumns(DbConnection &con, const QString &baseTableName,
                                   const QList<const DbFieldDescr *> &append_fields, LQError &err) const;
    virtual void dbTableRemoveColumns(DbConnection &con, const QString &baseTableName, const QStringList &drop_columns, LQError &err) const;
    virtual void dbTableRemoveColumnConstrains(DbConnection &con, const QString &baseTableName, const QString &columnName, LQError &err) const;
    virtual void dbTableChangeColumnType(DbConnection &con, const QString &tableName,
                                         const QString &column, const DbTypeDescr &newType, LQError &err) const = 0;
    virtual void dbTableRenameColumn(DbConnection &con, const DbTableDescr &table, const QString &oldName, const QString &newName, LQError &err) const = 0;
    virtual void dbTableRename(DbConnection &con, const QString &from, const QString &to, LQError &err) const = 0;

    virtual void dbCreateFK(DbConnection &con, const QString &tableName, const DbFKeyDescr &fk, LQError &err) const;
    virtual void dbDropFK(DbConnection &con, const QString &table, const QString &fkName, LQError &err) const;
    virtual void dbCreateIdx(DbConnection &con, const QString &tableName, const DbIndexDescr &dbIndex, LQError &err) const;
};

#endif // DBTYPEadapter_H
