#include "DbTypeAdapterMySQL.h"
#include "db/DbQuery.h"
#include "db/DbConnection.h"
#include "db/DbColumnInfo.h"
#include "db/DbLockInfo.h"
#include "db/tabledescr/DbTableDescr.h"
#include "db/tabledescr/DbFieldDescr.h"
#include "db/tabledescr/DbTypeDescr.h"
#include <QStringBuilder>
#include <memory>

const DbTypeAdapterMySQL &DbTypeAdapterMySQL::instance() {
    static const DbTypeAdapterMySQL adapter;
    return adapter;
}

QString DbTypeAdapterMySQL::dbFieldType(const DbTypeDescr &type) const {
    switch (type.type()) {
        case DbTypeDescr::Type::BigInteger:
            return QStringLiteral("BIGINT");
        case DbTypeDescr::Type::Integer:
            return QStringLiteral("INT");
        case DbTypeDescr::Type::SmallInteger:
            return QStringLiteral("smallint");
        case DbTypeDescr::Type::Float:
            return QStringLiteral("FLOAT");
        case DbTypeDescr::Type::Double:
            return QStringLiteral("DOUBLE");
        case DbTypeDescr::Type::String:
            return QString("VARCHAR(%1)").arg(type.len() < 0 ? 512 : type.len());
        case DbTypeDescr::Type::LongString:
            return QStringLiteral("TEXT");
        case DbTypeDescr::Type::LongText:
            return  QStringLiteral("LONGTEXT");
        case DbTypeDescr::Type::DateTime:
            return QStringLiteral("DATETIME") % (type.len() < 0 ? QString() : QString("(%1)").arg(type.len()));
        case DbTypeDescr::Type::Date:
            return QStringLiteral("Date");
        case DbTypeDescr::Type::Time:
            return QStringLiteral("TIME") % (type.len() < 0 ? QString() : QString("(%1)").arg(type.len()));
        case DbTypeDescr::Type::Image:
        case DbTypeDescr::Type::Blob:
            return QStringLiteral("BLOB");
        case DbTypeDescr::Type::Color:
            return QStringLiteral("INT");
        case DbTypeDescr::Type::Bool:
            return QStringLiteral("BOOLEAN");
        case DbTypeDescr::Type::AutoID:
            return QStringLiteral("BIGINT AUTO_INCREMENT");
        default:
            return QString{};
    }
}

bool DbTypeAdapterMySQL::dbSameType(const DbFieldDescr &field, const DbColumnInfo &info) const {
    bool ret = false;
    if (field.type().type() == DbTypeDescr::Type::AutoID) {
        if ((info.type().toLower() == QLatin1String("bigint")) &&
                (info.extra().toLower().contains(QLatin1String("auto_increment")))) {
            ret = true;
        }
    } else if ((field.type().type() == DbTypeDescr::Type::Bool) &&
               (info.type().toLower() == QLatin1String("tinyint"))) {
        /* boolean и tinyint эквиваленты в mysql/mariadb */
        ret = true;
    } else {
        ret = DbTypeAdapter::dbSameType(field, info);
    }
    return ret;
}


QString DbTypeAdapterMySQL::dbFieldTimeAddSecs(const QString &field, int secs) const {
    return QString("DATE_ADD(%1, INTERVAL %2 SECOND)").arg(field).arg(secs);
}

QString DbTypeAdapterMySQL::datetimeExtractPart(const QString &value, const DateTimePart &part) const {
    /* https://mariadb.com/kb/en/library/extract/ */
    QString partText;
    switch(part) {
        case DateTimePart_Hour: partText    = QStringLiteral("hour"); break;
        case DateTimePart_Minute: partText  = QStringLiteral("minute"); break;
        case DateTimePart_Second: partText  = QStringLiteral("second"); break;
        case DateTimePart_Year: partText    = QStringLiteral("year"); break;
        case DateTimePart_Quarter: partText = QStringLiteral("quarter"); break;
        case DateTimePart_Month: partText   = QStringLiteral("month"); break;
        case DateTimePart_Week: partText    = QStringLiteral("week"); break;
        case DateTimePart_Day: partText     = QStringLiteral("day"); break;
    }
    return QString("EXTRACT %1 FROM %2").arg(partText, value);
}

QString DbTypeAdapterMySQL::dbSelectLimit(const QString &req, int cnt) const {
    return req % QString(" LIMIT %1").arg(cnt);
}

QString DbTypeAdapterMySQL::dbGetLastIDQueryText() const {
    return QStringLiteral("SELECT LAST_INSERT_ID();");
}



void DbTypeAdapterMySQL::dbLockAcquire(DbConnection &con,const DbLockInfo &lock, unsigned tout, LQError &err) const {
   const std::unique_ptr<DbQuery> query{con.createQuery()};
   query->exec(QString("SELECT GET_LOCK('%1',%2);").arg(lock.name()).arg(1000*tout), tr("Lock %1 acquire").arg(lock.name()), err);
}

void DbTypeAdapterMySQL::dbLockRelease(DbConnection &con, const DbLockInfo &lock, LQError &err) const {
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(QString("SELECT RELEASE_LOCK('%1');").arg(lock.name()), tr("Lock %1 release").arg(lock.name()), err);
}


QString DbTypeAdapterMySQL::getDatabasesQueryString(const QString &dbname) const {
    return QString("SHOW DATABASES LIKE '%1'").arg(dbname);
}

void DbTypeAdapterMySQL::dbTableRenameColumn(DbConnection &con,  const DbTableDescr &table,
                                             const QString &oldName, const QString &newName, LQError &err) const {

    const QString qtext {QString("ALTER TABLE %1 CHANGE %2 %3")
                .arg(con.dbTableFullName(table.name()))
                .arg(oldName)
                .arg(dbCreateColumnText(con.connectionParams(), table.fieldDescr(newName)))};

    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(qtext,  tr("Column %1 rename").arg(oldName), err);
}

void DbTypeAdapterMySQL::dbTableAddColumns(DbConnection &con, const QString &baseTableName,
                                           const QList<const DbFieldDescr *> &append_fields, LQError &err) const {

    QString queryText {QString("ALTER TABLE %0 ADD (")
                .arg(con.dbTableFullName(baseTableName))};

    for (int f {0}; f < append_fields.size(); ++f) {
        queryText += dbCreateColumnText(con.connectionParams(), append_fields.at(f));
        if (f != (append_fields.size() - 1)) {
            queryText += QLatin1String(", ");
        } else {
            queryText += QLatin1String(");");
        }
    }

    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(queryText, tr("Addition columns to table %1")
                .arg(con.dbTableFullName(baseTableName)), err);
}

void DbTypeAdapterMySQL::dbTableRemoveColumns(DbConnection &con, const QString &baseTableName,
                                              const QStringList &drop_columns, LQError &err) const {
    QString queryText {QString("ALTER TABLE %0 ")
            .arg(con.dbTableFullName(baseTableName))};

    for (int col {0}; (col < drop_columns.size()) && err.isSuccess(); ++col) {
        dbTableRemoveColumnConstrains(con, baseTableName, drop_columns.at(col), err);
        if (err.isSuccess()) {
            queryText += QString("DROP COLUMN %1").arg(drop_columns.at(col));
            if (col != (drop_columns.size()-1)) {
                queryText += QLatin1String(", ");
            } else {
                queryText += QLatin1String(";");
            }
        }
    }

    if (err.isSuccess()) {
        const std::unique_ptr<DbQuery> query{con.createQuery()};
        query->exec(queryText, tr("Removal columns from table %1").
                    arg(con.dbTableFullName(baseTableName)), err);
    }
}

void DbTypeAdapterMySQL::dbTableChangeColumnType(DbConnection &con, const QString &tableName,
                                                 const QString &column, const DbTypeDescr &type, LQError &err) const {

    const std::unique_ptr<DbQuery> query{con.createQuery()};
    if (type.type() == DbTypeDescr::Type::AutoID) {
        query->exec("set foreign_key_checks = 0;",
                    tr("Disable foreing key for auto id column %1")
                    .arg(column), err);
    }

    if (err.isSuccess()) {
        const QString queryText {QString("ALTER TABLE %0 CHANGE %1 %1 %2;")
                .arg(con.dbTableFullName(tableName),
                     column,
                     dbFieldType(type))};
        query->exec(queryText, tr("Change column %2 type of table %1")
                    .arg(tableName, column), err);
    }

    if (type.type() == DbTypeDescr::Type::AutoID) {
        LQError en_err;
        query->exec(QStringLiteral("set foreign_key_checks = 1;"),
                    tr("Enable foreing key for auto id column %1").arg(column),
                    en_err);
        err += en_err;
    }
}

void DbTypeAdapterMySQL::dbTableRename(DbConnection &con, const QString &oldName, const QString &newName, LQError &err) const {
    const QString queryText {QString("RENAME TABLE  `%1` TO  `%2`").arg(oldName, newName)};
    const std::unique_ptr<DbQuery> query{con.createQuery()};
    query->exec(queryText, tr("Rename table from %1 to %2")
                .arg(oldName, newName), err);
}
