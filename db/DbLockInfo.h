#ifndef DBLOCKINFO_H
#define DBLOCKINFO_H

#include <QString>

class DbLockInfo {
public:
    DbLockInfo(int init_id, const QString &init_name);

    const QString &name() const {return m_name;}
    int id() const {return m_id;}

private:
    int m_id;
    QString m_name;
};

#endif // DBLOCKINFO_H
