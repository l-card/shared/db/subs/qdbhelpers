#ifndef DBQTSQLCONNECTION_H
#define DBQTSQLCONNECTION_H

#include "../DbConnection.h"
#include <QSqlDatabase>
#include <QHash>
class QSqlError;

class DbQtSqlConnection : public DbConnection {
    Q_OBJECT
public:
    explicit DbQtSqlConnection(QObject *parent = nullptr, const QString &conName = QStringLiteral("Default"));

    const QString &name() const {return m_conName;}

    bool isOpen() const override;
    DbQuery *createQuery() override;
    QString queryPlaceholder(int pos) const override;

    QSqlDatabase &qtsqldb() {return m_db;}

    static QString errorText(const QSqlError &err);
protected:
    void protOpen(const DbTypeAdapter &adapter, const DbConnectionParams &conParams, LQError &err) override ;
    void protClose(LQError &err) override;
private:
    QSqlDatabase m_db;
    QString m_conName;
    QHash<QString, QSqlDatabase> m_dbTypeList;
};

#endif // DBQTSQLCONNECTION_H
