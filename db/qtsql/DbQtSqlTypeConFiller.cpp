#include "DbQtSqlTypeConFiller.h"
#include "db/DbConnectionParams.h"
#include "db/adapters/DbTypeAdapterMSSQL.h"
#include "db/adapters/DbTypeAdapterMySQL.h"
#include "db/adapters/DbTypeAdapterPostgreSQL.h"
#include <QSqlDatabase>

const DbQtSqlTypeConFiller &DbQtSqlTypeConFiller::mysql() {
    static const class DbQtSqlTypeConFillerMySQL : public DbQtSqlTypeConFiller {
        const DbTypeAdapter &dbAdapter() const override {return DbTypeAdapterMySQL::instance();}
        QString qtDriverName() const override {return QStringLiteral("QMYSQL");}
        void fillConnectionPoarams(QSqlDatabase &sqlDb, const DbConnectionParams &conParams) const  override {
            sqlDb.setHostName(conParams.serverName());
            sqlDb.setDatabaseName(conParams.databaseName());
            sqlDb.setPort(conParams.port());
            sqlDb.setUserName(conParams.user());
            sqlDb.setPassword(conParams.password());
        }
    } fl;
    return fl;
}

const DbQtSqlTypeConFiller &DbQtSqlTypeConFiller::mssql() {
    static const class DbQtSqlTypeConFillerMSSQL : public DbQtSqlTypeConFiller {
        const DbTypeAdapter &dbAdapter() const override {return DbTypeAdapterMSSQL::instance();}
        QString qtDriverName() const override {return QStringLiteral("QODBC");}
        void fillConnectionPoarams(QSqlDatabase &sqlDb, const DbConnectionParams &conParams) const  override {
#ifdef Q_OS_WIN
            /* по умолчанию используем старый драйвер ODBC */
            /** @todo использование более нового драйвера. приводит к проблемам с конвертацией
             * времени и/или дат  - проверить работу */
            QString driver = "SQL Server";
            QString con_str = QString("Driver={%1}; Server=%2; CharSet=UTF8; Database=%3")
                    .arg(driver).arg(conParams.serverName()).arg(conParams.databaseName());
            if (conParams.user().isEmpty()) {
                con_str.append(";Trusted_Connection=yes");
            }
#else
            QString con_str = conParams.databaseName();
#endif

            sqlDb.setDatabaseName(con_str);
            sqlDb.setConnectOptions(QString("SQL_ATTR_CONNECTION_TIMEOUT=%1;SQL_ATTR_LOGIN_TIMEOUT=%1")
                                    .arg(QString::number(DbTypeAdapterMSSQL::connection_tout)));
            sqlDb.setUserName(conParams.user());
            sqlDb.setPassword(conParams.password());
        }
    } fl;
    return fl;
}


const DbQtSqlTypeConFiller &DbQtSqlTypeConFiller::psql() {
    static const class DbQtSqlTypeConFillerMySQL : public DbQtSqlTypeConFiller {
        const DbTypeAdapter &dbAdapter() const override {return DbTypeAdapterPostgreSQL::instance();}
        QString qtDriverName() const override {return QStringLiteral("QPSQL");}
        void fillConnectionPoarams(QSqlDatabase &sqlDb, const DbConnectionParams &conParams) const  override {
            sqlDb.setHostName(conParams.serverName());
            sqlDb.setDatabaseName(conParams.databaseName());
            sqlDb.setPort(conParams.port());
            sqlDb.setUserName(conParams.user());
            sqlDb.setPassword(conParams.password());
        }
    } fl;
    return fl;
}


const QList<const DbQtSqlTypeConFiller *> &DbQtSqlTypeConFiller::supportedList() {
    static const QList<const DbQtSqlTypeConFiller *> list {
        &mysql(),
        &mssql(),
        &psql()
    };
    return list;
}
