#ifndef DBQTSQLQUERY_H
#define DBQTSQLQUERY_H

#include "../DbQuery.h"
#include <QSqlQuery>
class DbQtSqlConnection;

class DbQtSqlQuery : public DbQuery {
    Q_OBJECT
public:
    explicit DbQtSqlQuery(DbQtSqlConnection *con);

    void set(int pos, qlonglong val) override;
    void set(int pos, int val) override;
    void set(int pos, double val) override;
    void set(int pos, bool val) override;
    void set(int pos, const QString &val) override;
    void set(int pos, const QDateTime &datetime) override;
    void set(int pos, const QByteArray &val) override;


    void get(int pos, qlonglong &val, bool *isNull = nullptr) const override;
    void get(int pos, int &val, bool *isNull = nullptr) const override;
    void get(int pos, double &val, bool *isNull = nullptr) const override;
    void get(int pos, bool &val, bool *isNull = nullptr) const override;
    void get(int pos, QString &val, bool *isNull = nullptr) const override;
    void get(int pos, QDateTime &val, bool *isNull = nullptr) const override;
    void get(int pos, QByteArray &val, bool *isNull = nullptr) const override;


    bool getLastId(qlonglong &id) override;


protected:
    int protPrepare(const QString &str, QString &errStr) override;
    int protExec(QString &errStr) override;
    bool protNext() override;
    int protFinish(QString &errStr) override;
private:
    QVariant getVariant(int pos, bool *isNull) const;
    int getErrorCode(QString &errStr);

    QSqlQuery m_qtquery;
};

#endif // DBQTSQLQUERY_H
