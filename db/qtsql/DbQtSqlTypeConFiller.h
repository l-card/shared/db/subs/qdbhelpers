#ifndef DBQTSQLTYPECONFILLER_H
#define DBQTSQLTYPECONFILLER_H

#include <QString>
class DbTypeAdapter;
class QSqlDatabase;
class DbConnectionParams;

/* Устанавливает соответствие между типами баз данных и поддержкой их классами Qt.
 * Выполняет инициализаю QSqlDatabase, зависимую от типа базы данных */
class DbQtSqlTypeConFiller {
public:
    virtual ~DbQtSqlTypeConFiller() {}

    virtual const DbTypeAdapter &dbAdapter() const = 0;
    virtual QString qtDriverName() const = 0;
    virtual void fillConnectionPoarams(QSqlDatabase &sqlDb, const DbConnectionParams &conParams) const = 0;

    static const DbQtSqlTypeConFiller &mysql();
    static const DbQtSqlTypeConFiller &mssql();
    static const DbQtSqlTypeConFiller &psql();

    static const QList<const DbQtSqlTypeConFiller *> &supportedList();
};

#endif // DBQTSQLTYPECONFILLER_H
