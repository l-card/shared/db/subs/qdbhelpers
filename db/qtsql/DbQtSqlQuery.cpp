#include "DbQtSqlQuery.h"
#include "DbQtSqlConnection.h"
#include "db/adapters/DbTypeAdapter.h"
#include <QVariant>
#include <QSqlError>

DbQtSqlQuery::DbQtSqlQuery(DbQtSqlConnection *con) : DbQuery{con}, m_qtquery{con->qtsqldb()} {
    m_qtquery.setForwardOnly(true);
}


void DbQtSqlQuery::set(int pos, qlonglong val) {
    m_qtquery.bindValue(pos, val);
}

void DbQtSqlQuery::set(int pos, int val) {
    m_qtquery.bindValue(pos, val);
}

void DbQtSqlQuery::set(int pos, double val) {
    if (!qIsFinite(val)) {
        m_qtquery.bindValue(pos, QVariant{QVariant::Type::Double});
    } else {
        float fval = static_cast<float>(val);
        m_qtquery.bindValue(pos, static_cast<double>(fval));
    }
}

void DbQtSqlQuery::set(int pos, bool val) {
    m_qtquery.bindValue(pos, val);
}


void DbQtSqlQuery::set(int pos, const QString &val) {
    m_qtquery.bindValue(pos, val);
}

void DbQtSqlQuery::set(int pos, const QDateTime &datetime) {
    m_qtquery.bindValue(pos, datetime);
}

void DbQtSqlQuery::set(int pos, const QByteArray &val) {
    m_qtquery.bindValue(pos, val);
}

void DbQtSqlQuery::get(int pos, qlonglong &val, bool *isNull) const {
    val = getVariant(pos, isNull).toLongLong();
}

void DbQtSqlQuery::get(int pos, int &val, bool *isNull) const {
    val = getVariant(pos, isNull).toInt();
}

void DbQtSqlQuery::get(int pos, double &val, bool *isNull) const {
    val = getVariant(pos, isNull).toDouble();
}

void DbQtSqlQuery::get(int pos, bool &val, bool *isNull) const {
    val = getVariant(pos, isNull).toBool();
}

void DbQtSqlQuery::get(int pos, QString &val, bool *isNull) const {
    val = getVariant(pos, isNull).toString();
}

void DbQtSqlQuery::get(int pos, QDateTime &val, bool *isNull) const {
    val = getVariant(pos, isNull).toDateTime();
}

void DbQtSqlQuery::get(int pos, QByteArray &val, bool *isNull) const {
    val = getVariant(pos, isNull).toByteArray();
}

bool DbQtSqlQuery::getLastId(qlonglong &id) {
    bool ok {false};
    const bool has_next {m_qtquery.next()};
    if (has_next) {
        id = m_qtquery.value(0).toLongLong();
        ok = true;
    } else {
        m_qtquery.finish();
        id = m_qtquery.lastInsertId().toLongLong();
        ok = id != 0;
        if (!ok) {
            const QString queryText {connection().dbAdapter()->dbGetLastIDQueryText()};
            if (!queryText.isEmpty()) {
                if (m_qtquery.exec(queryText)) {
                    if (m_qtquery.next()) {
                        id = m_qtquery.value(0).toLongLong();
                        ok = true;
                    }
                }
            }
        }
    }
    return ok;
}


int DbQtSqlQuery::protPrepare(const QString &str, QString &errStr) {
    int err_code {0};
    if (!m_qtquery.prepare(str)) {
        err_code = getErrorCode(errStr);
    }
    return err_code;
}

int DbQtSqlQuery::protExec(QString &errStr) {
    int err_code {0};
    if (!m_qtquery.exec()) {
        err_code = getErrorCode(errStr);
    }
    return err_code;
}

bool DbQtSqlQuery::protNext() {
    return m_qtquery.next();
}

int DbQtSqlQuery::protFinish(QString &errStr) {
    Q_UNUSED(errStr)
    m_qtquery.finish();
    return 0;
}

QVariant DbQtSqlQuery::getVariant(int pos, bool *isNull) const {
    const QVariant val {m_qtquery.value(pos)};
    if (isNull != nullptr)
        *isNull = val.isNull();
    return val;
}

int DbQtSqlQuery::getErrorCode(QString &errStr) {
    QSqlError sqlerr = m_qtquery.lastError();
    errStr = DbQtSqlConnection::errorText(sqlerr);
    return sqlerr.number() == 0 ? - 1 : sqlerr.number();
}
