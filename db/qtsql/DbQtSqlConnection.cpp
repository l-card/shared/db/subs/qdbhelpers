#include "DbQtSqlConnection.h"
#include "DbQtSqlQuery.h"
#include "DbQtSqlTypeConFiller.h"
#include "db/DbStdErrors.h"
#include "db/adapters/DbTypeAdapter.h"
#include <QSqlError>

DbQtSqlConnection::DbQtSqlConnection(QObject *parent, const QString &conName) :
    DbConnection{parent}, m_conName{conName} {

}

bool DbQtSqlConnection::isOpen() const {
    return m_db.isOpen();
}

DbQuery *DbQtSqlConnection::createQuery() {
    return new DbQtSqlQuery(this);
}

QString DbQtSqlConnection::queryPlaceholder(int pos) const {
    return QString(":%1").arg(pos);
}

QString DbQtSqlConnection::errorText(const QSqlError &err) {
    return err.databaseText();
}

void DbQtSqlConnection::protOpen(const DbTypeAdapter &adapter, const DbConnectionParams &conParams, LQError &err) {

    const QList<const DbQtSqlTypeConFiller *> &conFillers {DbQtSqlTypeConFiller::supportedList()};
    const auto &it {std::find_if(conFillers.cbegin(), conFillers.cend(),
                                 [&adapter](const DbQtSqlTypeConFiller *conFiller){return &conFiller->dbAdapter() == &adapter;})};
    const DbQtSqlTypeConFiller *dbConFiller {it != conFillers.cend() ? *it : nullptr};

    if (dbConFiller) {
        const QString drvName {dbConFiller->qtDriverName()};
        if (!m_dbTypeList.contains(drvName)) {
            m_dbTypeList[drvName] = QSqlDatabase::addDatabase(drvName, QString("%1.%2").arg(m_conName, drvName));
        }
        m_db = m_dbTypeList[drvName];
        dbConFiller->fillConnectionPoarams(m_db, conParams);
        if (!m_db.open()) {
            err = DbStdErrors::dbErrorOpen(errorText(m_db.lastError()));
        }
    } else {
        err = DbStdErrors::dbErrorNoTypeAdapter(adapter.typeName());
    }
}

void DbQtSqlConnection::protClose(LQError &err) {
    m_db.close();
}

