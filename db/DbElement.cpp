#include "DbElement.h"
#include "DbElementSerializer.h"
#include "DbConnection.h"
#include "DbQuery.h"
#include "DbRemoveQueue.h"
#include "LQError.h"

DbElement::DbElement(QObject *parent) : QObject{parent} {

}

bool DbElement::isModified() const {
    bool ret {(m_state != ModifyState::Unmodified) && !m_markForDeletion};
    if (!ret) {
        for (DbElement *child : m_children) {
            if (child->isModified()) {
                ret = true;
                break;
            }
        }
    }
    return ret;
}

void DbElement::dbLoad(const DbQuery &query, LQError &err) {
    serializer().elementLoad(query, *this, err);
    if (err.isSuccess())
        m_state = ModifyState::Unmodified;
}

void DbElement::dbSave(DbConnection &con, LQError &err) {
    if (m_state != ModifyState::Unmodified) {
        savePrepare(err);
        if (err.isSuccess())
            serializer().elementSave(con, *this, err);
        if (err.isSuccess()) {
            savePostProc(err);
        }

        if (err.isSuccess()) {
            m_state = ModifyState::Unmodified;
        }
    }

    for (DbElement *child : qAsConst(m_children)) {
        LQError child_err;
        child->dbSave(con, child_err);
        err += child_err;
    }
}

void DbElement::dbRemove(DbConnection &con, DbRemoveQueue *remQueue, LQError &err) {
    /** @note Подразумевается, что зависимые элементы идут после тех, от
     * которых они зависят. В этом случае сохранение идет в прямом порядке,
     * а удаления в обратном */
    for (QList<DbElement *>::iterator it {--m_children.end()}; it >= m_children.begin(); --it) {
        LQError child_err;
        (*it)->dbRemove(con, remQueue, child_err);
        err += child_err;
    }
    m_children.clear();
    if (!m_markForDeletion) {
        Q_EMIT dbAbautToBeRemoved();
        if (remQueue)
            remQueue->addRemovedElement(this);
        m_markForDeletion = true;
        Q_EMIT dbRemoved();

        if (!remQueue) {
            dbDelete(con, err);
        }
    }
}

void DbElement::dbDelete(DbConnection &con, LQError &err) {
    if (m_state != ModifyState::New) {
        serializer().elementDelete(con, *this, err);
        if (err.isSuccess()) {
            Q_EMIT dbDeleted();
        }
    }
}

void DbElement::addChild(DbElement *elem, int idx) {
    connect(elem, &DbElement::modified, this, &DbElement::modified);
    connect(elem, &DbElement::modifyStateChanged, this, &DbElement::modifyStateChanged);
    const bool was_modified {isModified()};
    if (idx < 0)
        idx = m_children.size();
    m_children.insert(idx, elem);
    Q_EMIT childAppend(elem);
    if (elem->isNew())
        setModified();
    if (!was_modified) {
        Q_EMIT modifyStateChanged();
    }
}

void DbElement::removeChild(DbConnection &con, DbElement *elem) {
    const int idx {m_children.indexOf(elem)};
    if (idx >= 0) {
        m_children.removeAt(idx);
        Q_EMIT childRemoved(elem);
        const bool was_modified {isModified()};
        LQError derr;
        elem->dbRemove(con, nullptr, derr);
        setModified();
        if (!was_modified) {
            Q_EMIT modifyStateChanged();
        }
    }
}

void DbElement::setModified() {
    if (m_state != ModifyState::New) {
        m_state = ModifyState::Modified;
        Q_EMIT modifyStateChanged();
    }
    Q_EMIT modified();
}
