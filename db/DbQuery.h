#ifndef DBQUERY_H
#define DBQUERY_H

#include <QObject>
#include <QDateTime>
class LQError;
class DbConnection;

class DbQuery : public QObject {
    Q_OBJECT
public:
    explicit DbQuery(DbConnection *con);
    virtual ~DbQuery() {}

    DbConnection &connection() const {return *m_con;}

    void prepare(const QString &str, LQError &err);
    void exec(LQError &err);
    void exec(const QString &text, const QString &opDescr, LQError &err);
    bool next();
    void finish(LQError &err);



    virtual void set(int pos, qlonglong val) = 0;
    virtual void set(int pos, int val) = 0;
    virtual void set(int pos, double val) = 0;
    virtual void set(int pos, bool val) = 0;
    virtual void set(int pos, const QString &val) = 0;
    virtual void set(int pos, const QDateTime &datetime) = 0;
    virtual void set(int pos, const QByteArray &val) = 0;


    virtual void get(int pos, qlonglong &val, bool *isNull = nullptr) const = 0;
    virtual void get(int pos, int &val, bool *isNull = nullptr) const = 0;
    virtual void get(int pos, double &val, bool *isNull = nullptr) const = 0;
    virtual void get(int pos, bool &val, bool *isNull = nullptr) const = 0;
    virtual void get(int pos, QString &val, bool *isNull = nullptr) const = 0;
    virtual void get(int pos, QDateTime &val, bool *isNull = nullptr) const = 0;
    virtual void get(int pos, QByteArray &val, bool *isNull = nullptr) const = 0;


    qlonglong getBigInt(int pos, bool *isNull = nullptr) const;
    int getInt(int pos, bool *isNull = nullptr) const;
    double getDouble(int pos, bool *isNull) const;
    QString getString(int pos, bool *isNull = nullptr) const;
    QDateTime getDateTime(int pos, bool *isNull = nullptr) const;
    QByteArray getBlob(int pos, bool *isNull = nullptr) const;

    void setOpDescription(const QString &descr);
    QString opDescription() const {return m_descr;}

    void setReopenOnError(bool en) {m_repen_on_err = en;}

    virtual bool getLastId(qlonglong &id) = 0;
protected:
    virtual int protPrepare(const QString &str, QString &errStr) = 0;
    virtual int protExec(QString &errStr) = 0;
    virtual bool protNext() = 0;
    virtual int protFinish(QString &errStr) = 0;

private:
    bool m_repen_on_err {true};
    DbConnection *m_con;
    QString m_descr;
};

#endif // DBQUERY_H
