#include "DbElementSerializer.h"
#include "db/tabledescr/DbTableDescr.h"
#include "db/tabledescr/DbFieldDescr.h"
#include "DbElement.h"
#include "DbQuery.h"
#include "DbConnection.h"
#include "LQError.h"
#include <QStringBuilder>
#include <memory>

DbElementSerializer::DbElementSerializer() {

}

void DbElementSerializer::elementLoad(const DbQuery &query, DbElement &elem, LQError &err) const {
    int pos {0};
    protLoad(query, elem, pos, err);
}

void DbElementSerializer::elementSave(DbConnection &con, DbElement &elem, LQError &err) const {
    elementSave(con, elem, saveFieldNames(), err);
}

void DbElementSerializer::elementSave(DbConnection &con, DbElement &elem, const QStringList &fields, LQError &err) const {
    QString queryText {saveString(con, elem, fields)};
    protSavePrepare(con, elem, queryText, err);
    if (err.isSuccess()) {
        const std::unique_ptr<DbQuery> query{con.createQuery()};
        query->setOpDescription(tr("Save elements to database %1")
                                .arg(con.dbTableFullName(elem.serializer().table().name())));
        query->prepare(queryText, err);

        if (err.isSuccess()) {
            int pos {0};
            protSave(*query, elem, pos, err);
        }

        if (err.isSuccess())
            query->exec(err);

        if (err.isSuccess())
            protSavePostProc(*query, elem, err);
     }
}

void DbElementSerializer::elementDelete(DbConnection &con, DbElement &elem, LQError &err) const {
    const std::unique_ptr<DbQuery> query(con.createQuery());
    query->exec(QString("DELETE FROM %1 %2;").arg(dbTableFullName(con), elementWhereString(con, elem)),
                tr("Element deletion from table %1").arg(dbTableFullName(con)),
                err);
}

QString DbElementSerializer::selectString(const DbConnection &con) const {
    return selectString(con, table());
}

QString DbElementSerializer::saveString(const DbConnection &con, const DbElement &elem, const QStringList &fields) const {
    QString ret;
    if (elem.modifyState() == DbElement::ModifyState::New) {
        ret = QString("INSERT INTO %1 (").arg(dbTableFullName(con));

        for (int i {0}; i < fields.size(); ++i) {
            ret += fields[i];
            if (i == (fields.size()-1)) {
                ret += QLatin1String(") VALUES (");
            } else {
                ret += QLatin1String(", ");
            }
        }

        for (int i {0}; i < fields.size(); ++i) {
            ret += con.queryPlaceholder(i);
            if (i == (fields.size() - 1)) {
                ret += QLatin1String(");");
            } else {
                ret += QLatin1String(", ");
            }
        }
    } else {
        ret = QString("UPDATE %1 SET ").arg(dbTableFullName(con));
        for (int i {0}; i < fields.size(); ++i) {
            ret += fields[i] % QLatin1String(" = ") % con.queryPlaceholder(i);
            if (i == (fields.size() - 1)) {
                ret += QLatin1Char(' ') % elementWhereString(con, elem) % QLatin1Char(';');
            } else {
                ret.append(QLatin1String(", "));
            }
        }
    }
    return ret;
}

QString DbElementSerializer::selectString(const DbConnection &con, const DbTableDescr &table, const QStringList &fields) {
    QString ret {"SELECT "};
    ret += (fields.isEmpty() ? fieldListString(table) : fieldListString(fields));
    ret += QLatin1String(" FROM ") % con.dbTableFullName(table.name()) % QLatin1Char(' ');
    return ret;
}

QString DbElementSerializer::fieldListString(const DbTableDescr &table) {
    return fieldListString(fieldNames(table));
}

QString DbElementSerializer::fieldListString(const QStringList &fieldsNames) {
    QString ret;
    for (int i {0}; i < fieldsNames.size(); ++i) {
        ret += fieldsNames.at(i);
        if (i != (fieldsNames.size() - 1)) {
            ret += QLatin1String(", ");
        }
    }
    return ret;
}


QStringList DbElementSerializer::fieldNames(const DbTableDescr &table) {
    QStringList  fields;
    const QList<const DbFieldDescr *> &fieldList {table.fields()};
    for (const DbFieldDescr *field : fieldList) {
        fields << field->fieldName();
    }
    return fields;
}

QStringList DbElementSerializer::fieldNames() const {
    return fieldNames(table());
}

QStringList DbElementSerializer::saveFieldNames() const {
    QStringList  fields;
    const QList<const DbFieldDescr *> &fieldList {table().fields()};
    for (const DbFieldDescr *field : fieldList) {
        if (field->defaultSave())
            fields << field->fieldName();
    }
    return fields;
}

QString DbElementSerializer::dbTableFullName(const DbConnection &con) const {
    return con.dbTableFullName(table().name());
}
