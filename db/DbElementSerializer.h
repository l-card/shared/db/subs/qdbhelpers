#ifndef DBELEMENTSERIALIZER_H
#define DBELEMENTSERIALIZER_H

#include <QObject>
#include <QStringList>
class LQError;
class DbTableDescr;
class DbQuery;
class DbElement;
class DbConnection;

class DbElementSerializer : public QObject {
    Q_OBJECT
public:
    DbElementSerializer();

    virtual const DbTableDescr &table() const = 0;

    void elementLoad(const DbQuery &query, DbElement &elem, LQError &err) const;
    void elementSave(DbConnection &con, DbElement &elem, LQError &err) const;
    void elementSave(DbConnection &con, DbElement &elem, const QStringList &fields, LQError &err) const;
    void elementDelete(DbConnection &con, DbElement &elem, LQError &err) const;


    virtual QString selectString(const DbConnection &con) const;
    virtual QString saveString(const DbConnection &con, const DbElement &elem,
                               const QStringList &fields) const;
    virtual QString elementWhereString(const DbConnection &con, const DbElement &elem) const = 0;



    static QString selectString(const DbConnection &con, const DbTableDescr &table,
                                const QStringList &fields = QStringList());
    static QString fieldListString(const DbTableDescr &table);
    static QString fieldListString(const QStringList &fieldsNames);

    static QStringList fieldNames(const DbTableDescr &table);
    QStringList fieldNames() const;
    virtual QStringList saveFieldNames() const;

    QString dbTableFullName(const DbConnection &con) const;
protected:    
    virtual void protLoad(const DbQuery &query, DbElement &elem, int &pos, LQError &err) const = 0;
    virtual void protSave(DbQuery &query, const DbElement &elem, int &pos, LQError &err) const = 0;
    virtual void protSavePrepare(DbConnection &con, const DbElement &elem, QString &queryText, LQError &err) const {}
    virtual void protSavePostProc(DbQuery &query, const DbElement &elem, LQError &err) const { }
};

#endif // DBELEMENTSERIALIZER_H
