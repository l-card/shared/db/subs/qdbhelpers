#include "DbConnection.h"
#include "DbElement.h"
#include "DbQuery.h"
#include "DbStdErrors.h"
#include "db/adapters/DbTypeAdapter.h"
#include "db/adapters/DbTypeAdapterMySQL.h"
#include "db/adapters/DbTypeAdapterMSSQL.h"
#include <memory>

DbConnection::DbConnection(QObject *parent) : QObject{parent} {

}


void DbConnection::open(const DbConnectionParams &conParams, const OpenOptions &opt, LQError &err) {

    LQError cerr;
    if (isOpen()) {
        close(cerr);
    }

    m_dbAdapter = getAdapter(conParams.databaseType());
    m_conParams = conParams;
    m_openOpts = opt;
    if (!m_dbAdapter) {
        err = DbStdErrors::dbErrorNoTypeAdapter(conParams.databaseType());
    } else {
        protOpen(*m_dbAdapter, conParams, err);
        /* при ошибке подключения пробуем проверить, нам не удалось связаться с сервером или
         * просто нет нужной базы в серевере (в последнем случае либо явно указываем, либо
         * можем создать - в зависимости от опции) */
        if (!err.isSuccess()) {
            DbConnectionParams tmpParams{conParams};
            /* Для проверки устанавиливаем связь с базой, котороя всегда должна быть
             * на сервере данного типа */
            tmpParams.setDatabaseName(m_dbAdapter->predefinedDatabaseName());
            LQError tmperr;
            protOpen(*m_dbAdapter, tmpParams, tmperr);
            if (tmperr.isSuccess()) {
                const std::unique_ptr<DbQuery> query{createQuery()};
                query->setReopenOnError(false);
                query->exec(m_dbAdapter->getDatabasesQueryString(conParams.databaseName()),
                            tr("Obtaining databases list"), tmperr);
                if (tmperr.isSuccess()) {
                    if (!query->next()) {
                        if (opt.enableDbCreate) {
                            query->exec(QString("CREATE DATABASE %1;").arg(conParams.databaseName()),
                                        tr("Database %1 creation").arg(conParams.databaseName()), tmperr);
                            err = tmperr;
                        } else {
                            err = DbStdErrors::dbErrorNoDatabase(conParams.databaseName());
                        }
                    }
                }
                protClose(cerr);
            }

            if (err.isSuccess()) {
                protOpen(*m_dbAdapter, conParams, err);
            }
        }

        if (err.isSuccess()) {
            if (m_dbAdapter->supportSchema()) {
                const std::unique_ptr<DbQuery> query{createQuery()};
                query->setReopenOnError(false);
                query->exec(QString("SELECT name FROM sys.schemas WHERE sys.schemas.name = '%1'")
                                 .arg(conParams.schema()),
                                 tr("Obtaining database schemas list"), err);
                if (err.isSuccess()) {
                    if (!query->next()) {
                        if (opt.enableDbCreate) {
                            query->exec(QString("CREATE SCHEMA %1;").arg(conParams.schema()),
                                        tr("Database schema %1 creation").arg(conParams.schema()), err);
                        } else {
                            err = DbStdErrors::dbErrorNoSchema(conParams.schema());
                        }
                    }
                }
            }
        }
    }
}

void DbConnection::close(LQError &err) {
    protClose(err);
}

void DbConnection::reopen(LQError &err) {
    close(err);
    open(m_conParams, m_openOpts, err);
}

QString DbConnection::dbTableFullName(const QString &baseName) const {
    return m_dbAdapter->dbTableFullName(m_conParams, baseName);
}



const DbTypeAdapter *DbConnection::getAdapter(const QString &dbName) const {
    static const QList<const DbTypeAdapter *> adapters {
        &DbTypeAdapterMySQL::instance(),
        &DbTypeAdapterMSSQL::instance()
    };
    const auto &it {std::find_if(adapters.cbegin(), adapters.cend(),
                                 [&dbName](const DbTypeAdapter *adapter){return adapter->typeName() == dbName;})};
    return it != adapters.cend() ? *it : nullptr;
}

