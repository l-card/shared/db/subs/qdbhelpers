#ifndef DBCONNECTION_H
#define DBCONNECTION_H

#include <QObject>
#include "DbConnectionParams.h"
class LQError;
class DbQuery;
class DbTypeAdapter;

class DbConnection : public QObject {
    Q_OBJECT
public:
    struct OpenOptions {
        OpenOptions() {};

        bool enableDbCreate {false};
    };

    DbConnection(QObject *parent = nullptr);

    void open(const DbConnectionParams &conParams, const OpenOptions &opt, LQError &err);
    void close(LQError &err);
    void reopen(LQError &err);
    virtual bool isOpen() const = 0;


    virtual DbQuery *createQuery() = 0;
    virtual QString queryPlaceholder(int pos) const= 0;



    const DbTypeAdapter *dbAdapter() const {return m_dbAdapter;}
    const DbConnectionParams &connectionParams() const {return m_conParams;}
    QString dbTableFullName(const QString &baseName) const;

protected:
    virtual void protOpen(const DbTypeAdapter &adapter, const DbConnectionParams &conParams, LQError &err) = 0;
    virtual void protClose(LQError &err) = 0;
private:


    const DbTypeAdapter *getAdapter(const QString &dbName) const;


    const DbTypeAdapter *m_dbAdapter;
    DbConnectionParams m_conParams;
    OpenOptions m_openOpts;
};

#endif // DBCONNECTION_H
