#ifndef DBSTDERRORS_H
#define DBSTDERRORS_H

#include "LQError.h"
#include <QObject>

class DbStdErrors : public QObject {
    Q_OBJECT
public:
    static LQError dbErrorNoTypeAdapter(const QString &type);
    static LQError dbErrorNoDatabase(const QString &name);
    static LQError dbErrorNoSchema(const QString &name);
    static LQError dbErrorOpen(const QString &descr);
    static LQError dbErrorUnuspVersion();
    static LQError dbErrorNotOpen();
private:
    static LQError error(int code, const QString &msg);
};

#endif // DBSTDERRORS_H
