#ifndef DBCOLUMNINFO_H
#define DBCOLUMNINFO_H


#include <QString>
class DbQuery;

class DbColumnInfo {
public:
    static QString fieldNameSchema() {return QStringLiteral("table_schema");}
    static QString fieldNameTableName() {return QStringLiteral("table_name");}
    static QString fieldNameColumnName() {return QStringLiteral("column_name");}
    static QString fieldNameDefaultValue() {return QStringLiteral("column_default");}
    static QString fieldNameDataType() {return QStringLiteral("data_type");}
    static QString fieldNameExtra() {return QStringLiteral("extra");}

    static QString fieldNamesString();

    explicit DbColumnInfo(const DbQuery &query);

    const QString &name() const {return m_name;}
    const QString &type() const {return m_type;}
    const QString &defaultValue() const {return m_default;}
    const QString &tableName() const {return m_tableName;}
    const QString &extra() const {return m_extra;}
private:
    QString m_schema;
    QString m_tableName;
    QString m_name;
    QString m_default;
    QString m_type;
    QString m_extra;
};

#endif // DBCOLUMNINFO_H
