#include "DbRemoveQueue.h"
#include "DbElement.h"
#include "LQError.h"

DbRemoveQueue::DbRemoveQueue() {

}

DbRemoveQueue::~DbRemoveQueue() {
    qDeleteAll(m_removedElements);
}

void DbRemoveQueue::addRemovedElement(DbElement *elem) {
    m_removedElements.append(elem);
}

void DbRemoveQueue::deleteRemovedElements(DbConnection &con, LQError &err) {
    /** @note Если элементы были удалены с несохраненными изменениями, то сперва
     * сохраняем эти изменения, т.к. иначе удаление других элементов может конфликтовать
     * со старыми параметрами этого элемента. При этом элементы в состоянии New
     * сохранять нет смысла */
    for (DbElement *elem : qAsConst(m_removedElements)) {
        if (elem->modifyState() == DbElement::ModifyState::Modified) {
            LQError serr;
            elem->dbSave(con, serr);
            err += serr;
        }
    }

    while (m_removedElements.size() && err.isSuccess()) {
        DbElement *elem {m_removedElements.at(0)};
        elem->dbDelete(con, err);
        if (err.isSuccess()) {
            m_removedElements.removeFirst();
            delete elem;
        }
    }
}
