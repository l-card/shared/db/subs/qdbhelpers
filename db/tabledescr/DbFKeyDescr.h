#ifndef DBFKEYDESCR_H
#define DBFKEYDESCR_H

class DbTableDescr;
#include <QString>

class DbFKeyDescr {
public:
    DbFKeyDescr(const QString &name, const QString &field,
                const QString &refTableName, const QString &refFieldName,
                const QString &action) :
        m_fkName{name},
        m_fieldName{field},
        m_refTableName{refTableName},
        m_refFieldName{refFieldName},
        m_action{action} {
    }

    const QString &name() const {return m_fkName;}
    const QString &fieldName() const {return m_fieldName;}
    const QString &refTableName() const {return m_refTableName;}
    const QString &refFieldName() const {return m_refFieldName;}
    const QString &action() const {return m_action;}
private:
    QString m_fkName;
    QString m_fieldName;
    QString m_refTableName;
    QString m_refFieldName;
    QString m_action;
};

#endif // DBFKEYDESCR_H
