#include "DbTableDescr.h"
#include "DbFieldDescr.h"
#include "DbIndexDescr.h"
#include "DbFKeyDescr.h"

DbTableDescr::~DbTableDescr() {
    qDeleteAll(m_fields);
    qDeleteAll(m_indexes);
    qDeleteAll(m_fks);
}

const DbFieldDescr *DbTableDescr::fieldDescr(const QString &fieldName) const {
    const auto &it {std::find_if(m_fields.cbegin(), m_fields.cend(),
                                 [&fieldName](const DbFieldDescr *field){return field->fieldName() == fieldName;})};
    return it != m_fields.cend() ? *it : nullptr;
}

int DbTableDescr::fieldPos(const QString &fieldName) const {
    int ret {-1};
    for (int f {0}; (f < m_fields.size()) && (ret < 0); ++f) {
        if (m_fields[f]->fieldName() == fieldName)
            ret = f;
    }
    return ret;
}
