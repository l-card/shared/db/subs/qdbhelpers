#ifndef SENSORDATAELEMDESCR_H
#define SENSORDATAELEMDESCR_H

#include <QString>
#include "DbTypeDescr.h"

class DbFieldDescr {
public:
    DbFieldDescr(const QString &fieldName, const DbTypeDescr &type,
                 bool isNullable = true,
                 const QString &defaultValue = QString{},
                 const QString &dbConstrains = QString{},
                 bool defaultSave = true);

    const QString &fieldName() const {return m_fieldName;}
    const DbTypeDescr &type() const {return m_type;}
    bool isNullable() const {return m_nullable;}
    const QString &defaultValue() const {return m_defaultValue;}
    const QString &dbConstrains() const {return m_dbConstrains;}
    bool defaultSave() const {return m_defaultSave;}

    bool sameName(const QString &name) const {
        return m_fieldName.compare(name, Qt::CaseSensitivity::CaseInsensitive) == 0;
    }
private:
    QString m_fieldName;

    const DbTypeDescr m_type;
    bool m_nullable;
    QString m_defaultValue;
    QString m_dbConstrains;

    bool m_defaultSave;
};



#endif // SENSORDATAELEMDESCR_H
