#include "DbFieldDescr.h"
#include "DbTableDescr.h"

DbFieldDescr::DbFieldDescr(const QString &fieldName, const DbTypeDescr &type,
                           bool nullable, const QString &defaultValue, const QString &dbConstrains,
                           bool saveOnDefault)
    : m_fieldName{fieldName},
      m_type{type},
      m_nullable{nullable},
      m_defaultValue{defaultValue},
      m_dbConstrains{dbConstrains},
      m_defaultSave{saveOnDefault} {

}

