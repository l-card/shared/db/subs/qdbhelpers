#ifndef DBINDEXDESCR_H
#define DBINDEXDESCR_H

#include <QString>
#include <QStringList>

class DbIndexDescr {
public:
    DbIndexDescr(const QString &name, const QStringList &fields, const QString &type = QString{}) :
        m_name{name}, m_fields{fields}, m_type{type} {

    }

    const QString& name() const {return m_name;}
    const QString& type() const {return m_type;}
    const QStringList& fields() const {return m_fields;}
private:
    QString m_name;
    QString m_type;
    QStringList m_fields;
};

#endif // DBINDEXDESCR_H
