#ifndef DbTypeDescr_H
#define DbTypeDescr_H


class DbTypeDescr {
public:
    enum class Type {
        Invalid,
        SmallInteger,
        Integer,
        BigInteger,
        Float,
        Double,
        String,
        LongString,
        LongText,
        DateTime,
        Date,
        Time,
        Blob,
        Image,
        Color,
        Bool,
        AutoID
    };

    DbTypeDescr(Type type, int len = -1);

    Type type() const {return m_type;}
    int len() const {return m_len;}

private:
    Type m_type;
    int m_len;
};

#endif // DbTypeDescr_H
