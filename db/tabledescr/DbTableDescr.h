#ifndef SENSORDATADESCR_H
#define SENSORDATADESCR_H

#include <QList>
class DbFieldDescr;
class DbFKeyDescr;
class DbIndexDescr;

class DbTableDescr {
public:
    virtual ~DbTableDescr();



    virtual QString name() const = 0;
    virtual QString tableDisplayName() const {return name();}
    const QList<const DbFieldDescr *> &fields() const {return m_fields;}
    const QList<const DbIndexDescr *> &indexes() const {return m_indexes;}
    const QList<const DbFKeyDescr *>  &foreignKeys() const {return m_fks;}



    const DbFieldDescr *fieldDescr(const QString &fieldName) const;
    int fieldPos(const QString &fieldName) const;

    void addField(const DbFieldDescr *field, int idx = -1) {
        if (idx < 0) {
            m_fields.append(field);
        } else {
            m_fields.insert(idx, field);
        }
    }
    void addIndex(const DbIndexDescr *index) {m_indexes.append(index);}
    void addForeignKey(const DbFKeyDescr *fk ) {m_fks.append(fk);}
protected:
    virtual void initialize() {}
private:
    QList<const DbFieldDescr*> m_fields;
    QList<const DbFKeyDescr *> m_fks;
    QList<const DbIndexDescr*> m_indexes;
};




#endif // SENSORDATADESCR_H



