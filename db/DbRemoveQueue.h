#ifndef DBREMOVEQUEUE_H
#define DBREMOVEQUEUE_H

#include <QList>
class LQError;
class DbElement;
class DbConnection;

class DbRemoveQueue {
public:
    DbRemoveQueue();
    virtual ~DbRemoveQueue();

    void addRemovedElement(DbElement *elem);
    void deleteRemovedElements(DbConnection &con, LQError &err);
private:
    QList<DbElement *> m_removedElements;
};

#endif // DBREMOVEQUEUE_H
