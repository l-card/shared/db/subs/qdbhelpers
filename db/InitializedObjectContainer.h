#ifndef INITIALIZED_OBJECT_CONATAINER_H
#define INITIALIZED_OBJECT_CONATAINER_H

template < class ObjType> class InitializedObjectContainer {
public:
    InitializedObjectContainer() {
        m_obj.initialize();
    }

    ObjType &object() {return m_obj;}

    operator ObjType&() {return m_obj;}
    operator const ObjType&() const {return m_obj;}
private:
    ObjType m_obj;
};

#endif // INITIALIZED_OBJECT_CONATAINER_H
