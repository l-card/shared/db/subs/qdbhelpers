#ifndef TUNNELDBELEMENT_H
#define TUNNELDBELEMENT_H

#include <QString>
#include <QObject>
#include <QList>
#include <QHash>
#include <QVariant>
#include <QMutex>
class LQError;
class DbDatabase;
class DbElementSerializer;
class DbQuery;
class DbConnection;
class DbRemoveQueue;

class DbElement : public QObject {
    Q_OBJECT
public:
    enum class ModifyState {
        New,
        Modified,
        Unmodified
    } ;

    explicit DbElement(QObject *parent = nullptr);


    ModifyState modifyState() const {return m_state;}
    bool isNew() const {return m_state == ModifyState::New;}
    virtual bool isModified() const;

    void dbLoad(const DbQuery &query, LQError &err);
    void dbSave(DbConnection &con, LQError &err);
    /* удаление элемента из системы.
     * Если передан remQueue, то элемент добавляется в список удаляемых объектов,
     * а непосредственно удаления из базы не происходит.
     * Если в remQueue передан нулевой указатель, то удаление идет сравзу в данном
     * методе после оповещения о удалении из системы */
    void dbRemove(DbConnection &con, DbRemoveQueue *remQueue, LQError &err);

    virtual const DbElementSerializer &serializer() const = 0;
protected:
    void addChild(DbElement *elem, int idx = -1);
    void removeChild(DbConnection &con, DbElement *elem);

    virtual void savePrepare(LQError &err) { }
    virtual void savePostProc(LQError &err) { }
Q_SIGNALS:
    void modified();
    void modifyStateChanged();

    void dbAbautToBeRemoved();
    void dbRemoved();
    void dbDeleted();

    void childAppend(DbElement *elem);
    void childRemoved(DbElement *elem);
protected Q_SLOTS:
    void setModified();
private:
    void dbDelete(DbConnection &con, LQError &err);

    ModifyState m_state {ModifyState::New};
    bool m_markForDeletion {false};
    QList<DbElement *> m_children;

    friend class DbRemoveQueue;
};

#endif // TUNNELDBELEMENT_H
