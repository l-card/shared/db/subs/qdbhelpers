#include "DbDatabaseInfoSerializer.h"
#include "DbDatabaseInfoTable.h"
#include "DbDatabaseInfo.h"
#include "db/DbQuery.h"
#include "LQError.h"

void DbDatabaseInfoSerializer::protLoad(const DbQuery &query, DbElement &elem, int &pos, LQError &err) const {
    DbStdIdElementSerializer::protLoad(query, elem, pos, err);
    if (err.isSuccess()) {
        DbDatabaseInfo &dbInfo {static_cast<DbDatabaseInfo &>(elem)};
        dbInfo.m_db_ver = query.getInt(pos++);
        dbInfo.m_db_compat_ver = query.getInt(pos++);
        dbInfo.m_lastUpdateTime = query.getDateTime(pos++);
    }
}

void DbDatabaseInfoSerializer::protSave(DbQuery &query, const DbElement &elem, int &pos, LQError &err) const {
    DbStdIdElementSerializer::protSave(query, elem, pos, err);
    if (err.isSuccess()) {
        const DbDatabaseInfo &dbInfo {static_cast<const DbDatabaseInfo &>(elem)};
        query.set(pos++, dbInfo.m_db_ver);
        query.set(pos++, dbInfo.m_db_compat_ver);
        query.set(pos++, dbInfo.m_lastUpdateTime);
    }
}
