#include "DbDatabaseInfoTable.h"
#include "db/tabledescr/DbFieldDescr.h"
#include "db/tabledescr/DbTypeDescr.h"

void DbDatabaseInfoTable::initialize() {
    DbStdIdElementTable::initialize();

    addField(new DbFieldDescr{fieldNameVersion(), DbTypeDescr::Type::Integer});
    addField(new DbFieldDescr{fieldNameCompatVersion(), DbTypeDescr::Type::Integer});
    addField(new DbFieldDescr{fieldNameLastUpdateTime(), DbTypeDescr::Type::DateTime});
}
