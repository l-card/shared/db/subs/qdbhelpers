#ifndef DBDATABASEINFO_H
#define DBDATABASEINFO_H

#include "db/elems/stdid/DbStdIdElement.h"
#include <QDateTime>

class DbDatabaseInfoTable;

class DbDatabaseInfo : public DbStdIdElement {
    Q_OBJECT
public:
    DbDatabaseInfo();

    virtual int appDbVersion() const = 0;
    virtual int appDbCompatVersion() const = 0;


    int dbVersion() const {return m_db_ver;}
    int dbCompatVersion() const {return m_db_compat_ver;}
    QDateTime dataUpdateTime() const {return m_lastUpdateTime;}

    void updateVersion();
    void updateTime();
    void clear();

    LQError checkVersion();
    bool upgradeAvailable();
private:
    int m_db_ver;
    int m_db_compat_ver;
    QDateTime m_lastUpdateTime;

    friend class DbDatabaseInfoSerializer;
};

#endif // DBDATABASEINFO_H
