#ifndef DBDATABASEINFOSERIALIZER_H
#define DBDATABASEINFOSERIALIZER_H

#include "db/elems/stdid/DbStdIdElementSerializer.h"
class DbDatabaseInfoTable;

class DbDatabaseInfoSerializer : public DbStdIdElementSerializer {
    Q_OBJECT
public:    

protected:
    void protLoad(const DbQuery &query, DbElement &elem, int &pos, LQError &err) const override;
    void protSave(DbQuery &query, const DbElement &elem, int &pos, LQError &err) const override;
};

#endif // DBDATABASEINFOSERIALIZER_H
