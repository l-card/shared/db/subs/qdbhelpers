#include "DbDatabaseInfo.h"
#include "DbDatabaseInfoTable.h"
#include "DbDatabaseInfoSerializer.h"
#include "db/DbStdErrors.h"
#include "db/InitializedObjectContainer.h"

DbDatabaseInfo::DbDatabaseInfo() {
    clear();
}


void DbDatabaseInfo::updateVersion() {
    if ((m_db_ver != appDbVersion()) || (m_db_compat_ver != appDbCompatVersion())) {
        m_db_ver = appDbVersion();
        m_db_compat_ver = appDbCompatVersion();
        updateTime();
    }
}

void DbDatabaseInfo::updateTime() {
    m_lastUpdateTime = QDateTime::currentDateTimeUtc();
    setModified();
}

void DbDatabaseInfo::clear() {
    m_db_ver = m_db_compat_ver = 0;
    m_lastUpdateTime = QDateTime();
}

LQError DbDatabaseInfo::checkVersion() {
    LQError err;
    if (m_db_compat_ver > appDbVersion()) {
        err = DbStdErrors::dbErrorUnuspVersion();
    }
    return err;
}

bool DbDatabaseInfo::upgradeAvailable() {
    return m_db_ver < appDbVersion();
}


