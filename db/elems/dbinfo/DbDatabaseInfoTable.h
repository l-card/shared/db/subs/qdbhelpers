#ifndef DBDATABASEINFOTABLE_H
#define DBDATABASEINFOTABLE_H

#include "db/elems/stdid/DbStdIdElementTable.h"

class DbDatabaseInfoTable : public DbStdIdElementTable {
public:

    static QString fieldNameVersion()           {return QStringLiteral("DbVersion");}
    static QString fieldNameCompatVersion()     {return QStringLiteral("DbCompatVersion");}
    static QString fieldNameLastUpdateTime()    {return QStringLiteral("LastUpdateTime");}

protected:
    void initialize() override;
    DbDatabaseInfoTable() {}
};

#endif // DBDATABASEINFOTABLE_H
