#include "DbStdIdElementSerializer.h"
#include "DbStdIdElementTable.h"
#include "db/InitializedObjectContainer.h"
#include "db/DbQuery.h"
#include "db/DbConnection.h"
#include "db/adapters/DbTypeAdapter.h"
#include "DbStdIdElement.h"
#include <QStringBuilder>
#include <memory>

DbStdIdElementSerializer::DbStdIdElementSerializer() {

}


void DbStdIdElementSerializer::protLoad(const DbQuery &query, DbElement &elem, int &pos, LQError &err) const {
    const DbStdIdElementTable &idTbl {static_cast<const DbStdIdElementTable &>(table())};
    if (idTbl.hasSurrogateId()) {
        DbStdIdElement &stdElem {static_cast<DbStdIdElement &>(elem)};
        stdElem.setID(query.getBigInt(pos++));
    }
}

void DbStdIdElementSerializer::protSave(DbQuery &query, const DbElement &elem, int &pos, LQError &err) const {
    const DbStdIdElementTable &idTbl {static_cast<const DbStdIdElementTable &>(table())};
    if (!idTbl.hasAutoIncId() && idTbl.hasSurrogateId()) {
        const DbStdIdElement &stdElem {static_cast<const DbStdIdElement &>(elem)};
        query.set(pos++, stdElem.m_id);
    }
}

void DbStdIdElementSerializer::protSavePrepare(DbConnection &con, const DbElement &elem, QString &queryText, LQError &err) const {
    const DbStdIdElementTable &idTbl {static_cast<const DbStdIdElementTable &>(table())};
    const DbStdIdElement &idElem {static_cast<const DbStdIdElement &>(elem)};

    if (idElem.isNew()) {
        if (idTbl.hasManualIncId()) {
            const std::unique_ptr<DbQuery> query{con.createQuery()};
            QString qtext {QString("SELECT %1 FROM %2 ORDER BY %1 DESC")
                            .arg(idTbl.idFieldName(), dbTableFullName(con))};
            qtext = con.dbAdapter()->dbSelectLimit(qtext, 1);
            query->exec(qtext, tr("Getting last element id from table %1").arg(dbTableFullName(con)), err);
            if (err.isSuccess()) {
                qlonglong lastId {0};
                if (query->next()) {
                    lastId = query->getBigInt(0);
                }
                idElem.setID(lastId + 1);
            }
        }

        if (idTbl.hasAutoIncId()) {
            queryText = con.dbAdapter()->dbInsertReturnID(queryText, idTbl.idFieldName());
        }
    }
}

void DbStdIdElementSerializer::protSavePostProc(DbQuery &query, const DbElement &elem, LQError &err) const {
    if (elem.isNew()) {
        const DbStdIdElementTable &idTbl {static_cast<const DbStdIdElementTable &>(table())};
        const DbStdIdElement &idElem {static_cast<const DbStdIdElement &>(elem)};
        if (idTbl.hasAutoIncId()) {
            qlonglong id;
            if (query.getLastId(id)) {
                idElem.setID(id);
            } else {
                err = LQError(-1, tr("Cannot get last record inserted id"));
            }
        }
    }
}

QString DbStdIdElementSerializer::elementWhereString(const DbConnection &con, const DbElement &elem) const {
    Q_UNUSED(con);
    const DbStdIdElementTable &idTbl {static_cast<const DbStdIdElementTable &>(table())};
    const DbStdIdElement &idElem {static_cast<const DbStdIdElement &>(elem)};
    return QLatin1String("WHERE ") % idTbl.idFieldName() % QLatin1String(" = ") % QString::number(idElem.id());
}



