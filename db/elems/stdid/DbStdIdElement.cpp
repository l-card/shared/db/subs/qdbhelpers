#include "DbStdIdElement.h"
#include "DbStdIdElementTable.h"

DbStdIdElement::DbStdIdElement(QObject *parent, qlonglong id) :
    DbElement{parent}, m_id{id} {

}

void DbStdIdElement::setID(qlonglong id) const {
    if (m_id != id) {
        m_id = id;
        Q_EMIT idChanged();
    }
}
