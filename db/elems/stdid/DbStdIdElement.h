#ifndef DBSTDIDELEMENT_H
#define DBSTDIDELEMENT_H

#include "db/DbElement.h"

class DbStdIdElement : public DbElement {
    Q_OBJECT
public:
    DbStdIdElement(QObject *parent = nullptr, qlonglong id = 0);

    virtual qlonglong id() const {return m_id;}
    virtual bool idValid() const {return m_id > 0;}
    virtual QString idString() const {return QString::number(m_id);}

Q_SIGNALS:
    void idChanged() const;
private:
    void setID(qlonglong id) const;

    mutable qlonglong m_id;

    friend class DbStdIdElementSerializer;
};

#endif // DBSTDIDELEMENT_H
