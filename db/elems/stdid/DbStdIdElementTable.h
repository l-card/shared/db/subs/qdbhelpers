#ifndef DBSTDIDELEMENTTABLE_H
#define DBSTDIDELEMENTTABLE_H

#include "db/tabledescr/DbTableDescr.h"
#include "db/tabledescr/DbTypeDescr.h"
#include "db/InitializedObjectContainer.h"

class DbStdIdElementTable : public DbTableDescr {
public:
    enum class IDType {
        AutoInc, /* id вычисляется автоматически базой. необходим способ получения id из базы при сохранении */
        ManualInc, /* id вычисляется программой при сохранении как макс. id + 1.
                            Может быть проблема при одновременном сохранении несколькими клиентами */
        ManualSet, /* id создается, но должен вручную устанавливаться наследуемым объектом */
        External  /* id определяется внешним образом. По сути отменяет работу всего кода из данного класса */
    };

    static QString           fieldNameID()   {return QStringLiteral("ID");}
    static const DbTypeDescr &fieldTypeManualID();
    static const DbTypeDescr &fieldTypeAutoID();


    virtual IDType idType() const {return IDType::AutoInc;}
    virtual QString idFieldName() const {return fieldNameID();}
    virtual const DbTypeDescr &idFieldType() const;
    virtual const DbTypeDescr &idRefFieldType() const;


    bool hasSurrogateId() const { return idType() != IDType::External;}
    bool hasAutoIncId() const {return idType() == IDType::AutoInc;}
    bool hasManualIncId() const {return idType() == IDType::ManualInc;}
    bool hasManualId() const {return (idType() == IDType::ManualInc) || (idType() == IDType::ManualSet);}
protected:
    DbStdIdElementTable();
    void initialize() override;
private:
    friend class InitializedObjectContainer<DbStdIdElementTable>;
};

#endif // DBSTDIDELEMENTTABLE_H
