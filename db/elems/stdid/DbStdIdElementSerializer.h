#ifndef DBSTDIDELEMENTSERIALIZER_H
#define DBSTDIDELEMENTSERIALIZER_H

#include "db/DbElementSerializer.h"
#include "db/tabledescr/DbFieldDescr.h"
#include <QString>



class DbStdIdElementSerializer : public DbElementSerializer {
    Q_OBJECT
public:
    DbStdIdElementSerializer();
protected:
    void protLoad(const DbQuery &query, DbElement &elem, int &pos, LQError &err) const override;
    void protSave(DbQuery &query, const DbElement &elem, int &pos, LQError &err) const override;
    void protSavePrepare(DbConnection &con, const DbElement &elem, QString &queryText, LQError &err) const override;
    void protSavePostProc(DbQuery &query, const DbElement &elem, LQError &err) const override;
    QString elementWhereString(const DbConnection &con, const DbElement &elem) const override;
};

#endif // DBSTDIDELEMENTSERIALIZER_H
