#include "DbStdIdElementTable.h"
#include "db/tabledescr/DbFieldDescr.h"

void DbStdIdElementTable::initialize() {
    if (hasSurrogateId()) {
        addField(new DbFieldDescr{idFieldName(), idFieldType(),
                                  false, QString(), QStringLiteral("PRIMARY KEY"),
                                  hasManualId()});
    }
}

const DbTypeDescr &DbStdIdElementTable::fieldTypeManualID() {
    static const DbTypeDescr type(DbTypeDescr::Type::BigInteger);
    return type;
}

const DbTypeDescr &DbStdIdElementTable::fieldTypeAutoID() {
    static const DbTypeDescr type(DbTypeDescr::Type::AutoID);
    return type;
}


const DbTypeDescr &DbStdIdElementTable::idFieldType() const {
    return hasAutoIncId() ? fieldTypeAutoID() : fieldTypeManualID();
}

const DbTypeDescr &DbStdIdElementTable::idRefFieldType() const {
    return fieldTypeManualID();
}



DbStdIdElementTable::DbStdIdElementTable() {

}
