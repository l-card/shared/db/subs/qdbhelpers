#include "DbStdErrors.h"

LQError DbStdErrors::error(int code, const QString &msg) {
    static const QString err_type {QStringLiteral("db")};
    return LQError{code, msg, err_type};
}


LQError DbStdErrors::dbErrorNoTypeAdapter(const QString &type) {
    return error(-1, tr("Not supported database type: %1").arg(type));
}

LQError DbStdErrors::dbErrorNoDatabase(const QString &name) {
    return error(-2, tr("Database %1 not exists").arg(name));
}

LQError DbStdErrors::dbErrorNoSchema(const QString &name) {
    return error(-3, tr("Database schema %1 not exists").arg(name));
}

LQError DbStdErrors::dbErrorOpen(const QString &descr) {
    return error(-4, tr("Cannot open database connection: %1").arg(descr));
}

LQError DbStdErrors::dbErrorUnuspVersion() {
    return error(-5, tr("Database structure version is not supported! Update your software!"));
}

LQError DbStdErrors::dbErrorNotOpen() {
    return error(-6, tr("Database connection is not open!"));
}


