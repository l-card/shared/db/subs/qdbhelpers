set(QDBHELPERS_DIR ${CMAKE_CURRENT_LIST_DIR})
set(QDBHELPERS_INCLUDE_DIRS  ${QDBHELPERS_DIR})

set(QDBHELPERS_SOURCES
     ${QDBHELPERS_DIR}/db/InitializedObjectContainer.h
     ${QDBHELPERS_DIR}/db/DbStdErrors.h
     ${QDBHELPERS_DIR}/db/DbStdErrors.cpp
     ${QDBHELPERS_DIR}/db/DbElement.h
     ${QDBHELPERS_DIR}/db/DbElement.cpp
     ${QDBHELPERS_DIR}/db/DbElementSerializer.h
     ${QDBHELPERS_DIR}/db/DbElementSerializer.cpp
     ${QDBHELPERS_DIR}/db/DbQuery.h
     ${QDBHELPERS_DIR}/db/DbQuery.cpp
     ${QDBHELPERS_DIR}/db/DbConnection.h
     ${QDBHELPERS_DIR}/db/DbConnection.cpp
     ${QDBHELPERS_DIR}/db/DbConnectionParams.h
     ${QDBHELPERS_DIR}/db/DbConnectionParams.cpp
     ${QDBHELPERS_DIR}/db/DbRemoveQueue.h
     ${QDBHELPERS_DIR}/db/DbRemoveQueue.cpp
     ${QDBHELPERS_DIR}/db/DbLockInfo.h
     ${QDBHELPERS_DIR}/db/DbLockInfo.cpp
     ${QDBHELPERS_DIR}/db/DbColumnInfo.h
     ${QDBHELPERS_DIR}/db/DbColumnInfo.cpp

     ${QDBHELPERS_DIR}/db/tabledescr/DbTableDescr.h
     ${QDBHELPERS_DIR}/db/tabledescr/DbTableDescr.cpp
     ${QDBHELPERS_DIR}/db/tabledescr/DbFieldDescr.h
     ${QDBHELPERS_DIR}/db/tabledescr/DbFieldDescr.cpp
     ${QDBHELPERS_DIR}/db/tabledescr/DbFKeyDescr.h
     ${QDBHELPERS_DIR}/db/tabledescr/DbIndexDescr.h
     ${QDBHELPERS_DIR}/db/tabledescr/DbTypeDescr.h
     ${QDBHELPERS_DIR}/db/tabledescr/DbTypeDescr.cpp

     ${QDBHELPERS_DIR}/db/adapters/DbTypeAdapter.h
     ${QDBHELPERS_DIR}/db/adapters/DbTypeAdapter.cpp
     ${QDBHELPERS_DIR}/db/adapters/DbTypeAdapterMSSQL.h
     ${QDBHELPERS_DIR}/db/adapters/DbTypeAdapterMSSQL.cpp
     ${QDBHELPERS_DIR}/db/adapters/DbTypeAdapterMySQL.h
     ${QDBHELPERS_DIR}/db/adapters/DbTypeAdapterMySQL.cpp
     ${QDBHELPERS_DIR}/db/adapters/DbTypeAdapterPostgreSQL.h
     ${QDBHELPERS_DIR}/db/adapters/DbTypeAdapterPostgreSQL.cpp


     ${QDBHELPERS_DIR}/db/elems/stdid/DbStdIdElement.h
     ${QDBHELPERS_DIR}/db/elems/stdid/DbStdIdElement.cpp
     ${QDBHELPERS_DIR}/db/elems/stdid/DbStdIdElementSerializer.h
     ${QDBHELPERS_DIR}/db/elems/stdid/DbStdIdElementSerializer.cpp
     ${QDBHELPERS_DIR}/db/elems/stdid/DbStdIdElementTable.h
     ${QDBHELPERS_DIR}/db/elems/stdid/DbStdIdElementTable.cpp

     ${QDBHELPERS_DIR}/db/elems/dbinfo/DbDatabaseInfo.h
     ${QDBHELPERS_DIR}/db/elems/dbinfo/DbDatabaseInfo.cpp
     ${QDBHELPERS_DIR}/db/elems/dbinfo/DbDatabaseInfoTable.h
     ${QDBHELPERS_DIR}/db/elems/dbinfo/DbDatabaseInfoTable.cpp
     ${QDBHELPERS_DIR}/db/elems/dbinfo/DbDatabaseInfoSerializer.h
     ${QDBHELPERS_DIR}/db/elems/dbinfo/DbDatabaseInfoSerializer.cpp

     ${QDBHELPERS_DIR}/db/qtsql/DbQtSqlQuery.h
     ${QDBHELPERS_DIR}/db/qtsql/DbQtSqlQuery.cpp
     ${QDBHELPERS_DIR}/db/qtsql/DbQtSqlConnection.h
     ${QDBHELPERS_DIR}/db/qtsql/DbQtSqlConnection.cpp
     ${QDBHELPERS_DIR}/db/qtsql/DbQtSqlTypeConFiller.h
     ${QDBHELPERS_DIR}/db/qtsql/DbQtSqlTypeConFiller.cpp
    )
    
include(${QDBHELPERS_DIR}/translations/cmake/qtranslation.cmake)
set(QDBHELPERS_TS_BASENAME qdbhelpers)
qtranslation_generate(QDBHELPERS_TRANSLATION_FILES ${QDBHELPERS_DIR}/translations ${QDBHELPERS_TS_BASENAME} ${QDBHELPERS_SOURCES})

set(QDBHELPERS_GENFILES ${QDBHELPERS_GENFILES} ${QDBHELPERS_TRANSLATION_FILES})
set(QDBHELPERS_FILES ${QDBHELPERS_SOURCES} ${QDBHELPERS_GENFILES})    
